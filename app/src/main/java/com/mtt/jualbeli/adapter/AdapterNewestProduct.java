package com.mtt.jualbeli.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.mtt.jualbeli.R;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.model.Product;
import com.mtt.jualbeli.utils.Tools;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class AdapterNewestProduct extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context ctx;
    private List<Product> items = new ArrayList<>();
    DecimalFormat decimalFormat;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Product obj, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView price;
        public ImageView image;
        public LinearLayout background;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            price = (TextView) v.findViewById(R.id.price);
            image = (ImageView) v.findViewById(R.id.image);
            background = (LinearLayout) v.findViewById(R.id.background);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_loading);
        }
    }

    public AdapterNewestProduct(Context ctx, List<Product> items) {
        this.ctx = ctx;
        this.items = items;
        decimalFormat = new DecimalFormat("#,###.##");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_newest, parent, false);
        vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof ViewHolder) {
            final Product p = items.get(position);
            ViewHolder vItem = (ViewHolder) holder;
            vItem.name.setText(p.name);
            //vItem.price.setText(p.price + " " + sharedPref.getInfoData().currency);

            vItem.price.setText("Rp. "+decimalFormat.format(p.price));

            //Tools.displayImageThumbnail(ctx, vItem.image, Constant.getURLimgProduct(p.image), 0.5f);
            Tools.displayImageOriginal(ctx, vItem.image, Constant.getURLimgProduct(p.image));
            vItem.background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(view, p, position);
                    }
                }
            });
        } else {
            ((AdapterProduct.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(List<Product> items) {
        this.items = items;
        notifyDataSetChanged();
    }


}