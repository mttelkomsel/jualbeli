package com.mtt.jualbeli.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.jualbeli.ActivityBuyer;
import com.mtt.jualbeli.ActivitySeller;
import com.mtt.jualbeli.R;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.fragment.FragmentSellerInvoice;
import com.mtt.jualbeli.fragment.FragmentSellerOrder;
import com.mtt.jualbeli.model.ProductOrder;
import com.mtt.jualbeli.model.ProductOrderDetail;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class AdapterSellerInvoice extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<ProductOrder> items = new ArrayList<>();
    private SharedPref sharedPref;
    private UserProfile userProfile;

    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private FragmentSellerInvoice fragment;


    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    public interface OnItemClickListener {
        void onItemClick(View view, ProductOrder obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterSellerInvoice(Context context, RecyclerView view, List<ProductOrder> items, FragmentSellerInvoice fragment) {
        this.items = items;
        ctx = context;
        sharedPref = new SharedPref(ctx);
        userProfile=sharedPref.getInfoUser();
        this.fragment=fragment;
        lastItemViewDetector(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView buyer;
        public TextView code;
        public TextView date;
        public TextView price;

        public TextView address;
        public TextView payment;
        public TextView date_ship;
        public TextView status;
        public TextView note;

        public TextView details;
       /* public TextView merchant_id;
        public TextView merchant_phone;
        public TextView merchant_name;
        public TextView product_name;
        public TextView amount;
        public TextView price_item;*/



        public ImageView send_call;
        public ImageView send_message;

        public Button bt_invoice, bt_confirm, bt_close;

        public LinearLayout lyt_parent, lyt_action;

        public ViewHolder(View v) {
            super(v);
            //buyer = (TextView) v.findViewById(R.id.buyer);
            code = (TextView) v.findViewById(R.id.code);
            date = (TextView) v.findViewById(R.id.date);
            price = (TextView) v.findViewById(R.id.price);
            address = (TextView) v.findViewById(R.id.address);
            payment = (TextView) v.findViewById(R.id.payment);
            date_ship = (TextView) v.findViewById(R.id.date_ship);
            status = (TextView) v.findViewById(R.id.status);
            note = (TextView) v.findViewById(R.id.note);
            details =(TextView) v.findViewById(R.id.details);
            send_call = (ImageView) v.findViewById(R.id.send_call);
            send_message = (ImageView) v.findViewById(R.id.send_message);
            bt_invoice = (Button) v.findViewById(R.id.bt_invoice);
            bt_confirm = (Button) v.findViewById(R.id.bt_confirm);
            bt_close = (Button) v.findViewById(R.id.bt_close);

            lyt_action = (LinearLayout) v.findViewById(R.id.lyt_action);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_loading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_seller_invoice, parent, false);
            vh = new AdapterSellerInvoice.ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new AdapterSellerInvoice.ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdapterSellerInvoice.ViewHolder) {
            AdapterSellerInvoice.ViewHolder vItem = (AdapterSellerInvoice.ViewHolder) holder;
            final ProductOrder c = items.get(position);
            vItem.code.setText(c.buyer +" ("+c.code+")");
            vItem.price.setText("Rp. "+(int)c.total_fees);
            vItem.status.setText(c.status);
            String mydate=Tools.getDate(c.date_ship);
            vItem.date_ship.setText("Waktu Kirim : "+mydate+", Pukul:"+c.time_ship);
            vItem.address.setText("Lokasi Kirim : "+c.address);
            vItem.payment.setText("Pembayaran : "+c.shipping);
            vItem.note.setText("Note : "+c.comment);
            List<ProductOrderDetail> pod = c.details;
            String _items="";
            double total_merchant=0;
            String merchant_name = "";
            String merchant_phone = "";

            final Long _admin_id=5L;
            final String _admin="Admin";
            int i = 0;
            for(i = 0; i<pod.size(); i++){
                double total=(pod.get(i).amount) *(pod.get(i).price_item);
               /* if(userProfile.id.equals(pod.get(i).merchant_id+"")) {
                    _items+="<b>"+(i+1)+". "+pod.get(i).product_name+" ("+pod.get(i).amount+") = Rp."+(int)total+"</b>";

                }else{
                    _items+=(i+1)+". "+pod.get(i).product_name+" ("+pod.get(i).amount+") = Rp."+(int)total;
                }*/
                if(userProfile.is_admin==1) {
                    _items += (i + 1) + ". " + pod.get(i).product_name + " (x" + pod.get(i).amount + ") = Rp." + (int) total;
                    _items +="\n By " + pod.get(i).merchant_name  + " ("+ pod.get(i).merchant_phone + ")";
                }else{
                    _items += (i + 1) + ". " + pod.get(i).product_name + " By " + pod.get(i).merchant_name + "(x" + pod.get(i).amount + ") = Rp." + (int) total;
                }

                if(userProfile.id.equals(pod.get(i).merchant_id+"")){
                    //product is owner by user
                    total_merchant=total_merchant+total;
                    _items+="====> TERHITUNG";
                    merchant_name=pod.get(i).merchant_name;
                    merchant_phone=pod.get(i).merchant_phone;
                }
                _items+="\n";
            }
            final double _total_merchant=total_merchant;
            final String _merchant_name=merchant_name;
            final String _merchant_phone=merchant_phone;
            if(userProfile.is_admin!=1) {
                vItem.price.setText(total_merchant + "");
            }
            _items+=(i+1)+". "+"Fee Delivery "+ "Rp."+c.fee_delivery;
            _items+="\n"+(i+1)+". "+"Infaq/Sodaqah "+ "Rp."+c.fee_infaq;

            vItem.details.setText("Items :\n\n"+_items);
             vItem.date.setText(Tools.getFormattedDateSimple(c.created_at));
           /* vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.dialogUpdateStatus(c);
                }

            });*/


            //annonymous user
            /*if(c.user_id==0){
                vItem.send_message.setVisibility(View.GONE);
            }else {*/
                //btn msg & call
            /*vItem.send_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String msg="Kode Order : "+c.code;
                    msg+="\nMerchant Name : "+_merchant_name;
                    msg+="\nMerchant Phone : "+_merchant_phone;
                    ((ActivitySeller)ctx).openMessage(msg, Long.parseLong(c.user_id+""), c.buyer);
                    //Toast.makeText(ctx.getApplicationContext(), "test-pesan", Toast.LENGTH_LONG).show();

                }
            });*/
            //}
            vItem.send_message.setVisibility(View.GONE);
            vItem.send_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ActivitySeller)ctx).openCall(c.phone);
                }
            });

            if(c.status.equals(ctx.getResources().getString(R.string.selesai))) {
                vItem.status.setText(ctx.getResources().getString(R.string.selesai_desc));
            }
            if(c.status.equals(ctx.getResources().getString(R.string.proses_ditagih))) {
                vItem.status.setText(ctx.getResources().getString(R.string.proses_ditagih_desc));
            }
            if(c.status.equals(ctx.getResources().getString(R.string.selesai_ditagih))) {
                vItem.status.setText(ctx.getResources().getString(R.string.selesai_ditagih_desc));
            }
                //button pay to merchant show
            if(c.status.equals(ctx.getResources().getString(R.string.selesai))||
                    c.status.equals(ctx.getResources().getString(R.string.proses_ditagih))){

                vItem.lyt_action.setVisibility(View.VISIBLE);
                vItem.bt_invoice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //fragment.dialogUpdateStatus(c);
                        //Toast.makeText(ctx.getApplicationContext(), "test-payment", Toast.LENGTH_LONG).show();
                        fragment.dialogUpdateStatus(c);
                    }
                });

                vItem.bt_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //fragment.dialogUpdateStatus(c);
                        String msg="Kode Order : "+c.code;
                        msg+="\nMerchant Name : "+_merchant_name;
                        msg+="\nMerchant Phone : "+_merchant_phone;

                        msg+="\nTotal : "+_total_merchant;
                        ((ActivitySeller)ctx).openMessage(msg, _admin_id, _admin);
                    }
                });

            }

            if(c.status.equals(ctx.getResources().getString(R.string.selesai_ditagih))){
                vItem.lyt_action.setVisibility(View.GONE);
            }

            if(userProfile.is_admin==1){
                vItem.bt_close.setVisibility(View.VISIBLE);
                vItem.bt_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fragment.dialogCloseStatus(c);
                    }
                });

            }

        } else {
               ((AdapterSellerInvoice.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<ProductOrder> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }


    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }


    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / Constant.PRODUCT_PER_REQUEST;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

}