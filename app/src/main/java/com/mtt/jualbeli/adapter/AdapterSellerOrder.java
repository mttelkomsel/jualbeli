package com.mtt.jualbeli.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.mtt.jualbeli.ActivityBuyer;
import com.mtt.jualbeli.fragment.FragmentSellerOrder;
import com.mtt.jualbeli.ActivitySeller;
import com.mtt.jualbeli.R;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.Order;
import com.mtt.jualbeli.model.ProductOrder;
import com.mtt.jualbeli.model.ProductOrderDetail;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class AdapterSellerOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<ProductOrder> items = new ArrayList<>();
    private SharedPref sharedPref;
    private UserProfile userProfile;

    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private FragmentSellerOrder fragment;


    private Context ctx;
    private OnItemClickListener mOnItemClickListener;


    public interface OnItemClickListener {
        void onItemClick(View view, ProductOrder obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterSellerOrder(Context context, RecyclerView view, List<ProductOrder> items, FragmentSellerOrder fragment) {
        this.items = items;
        ctx = context;
        sharedPref = new SharedPref(ctx);
        userProfile=sharedPref.getInfoUser();
        this.fragment=fragment;
        lastItemViewDetector(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView buyer;
        public TextView code;
        public TextView date;
        public TextView price;

        public TextView address;
        public TextView payment;
        public TextView date_ship;
        public TextView status;
        public TextView note;

        public TextView details;
       /* public TextView merchant_id;
        public TextView merchant_phone;
        public TextView merchant_name;
        public TextView product_name;
        public TextView amount;
        public TextView price_item;*/



        public ImageView send_call;
        public ImageView send_message;
        public Button bt_payment, bt_cancel, bt_confirm;

        public LinearLayout lyt_parent, lyt_action;


        public ViewHolder(View v) {
            super(v);
            //buyer = (TextView) v.findViewById(R.id.buyer);
            code = (TextView) v.findViewById(R.id.code);
            date = (TextView) v.findViewById(R.id.date);
            price = (TextView) v.findViewById(R.id.price);
            address = (TextView) v.findViewById(R.id.address);
            payment = (TextView) v.findViewById(R.id.payment);
            date_ship = (TextView) v.findViewById(R.id.date_ship);
            status = (TextView) v.findViewById(R.id.status);
            note = (TextView) v.findViewById(R.id.note);
            details =(TextView) v.findViewById(R.id.details);
            send_call = (ImageView) v.findViewById(R.id.send_call);
            send_message = (ImageView) v.findViewById(R.id.send_message);
            bt_cancel = (Button) v.findViewById(R.id.bt_cancel);
            bt_payment = (Button) v.findViewById(R.id.bt_payment);
            bt_confirm = (Button) v.findViewById(R.id.bt_confirm);

            lyt_action = (LinearLayout) v.findViewById(R.id.lyt_action);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_loading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_seller_order, parent, false);
            vh = new AdapterSellerOrder.ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new AdapterSellerOrder.ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdapterSellerOrder.ViewHolder) {
            AdapterSellerOrder.ViewHolder vItem = (AdapterSellerOrder.ViewHolder) holder;
            final ProductOrder c = items.get(position);
            vItem.code.setText(c.buyer +" ("+c.code+")");
            vItem.price.setText("Rp. "+(int)c.total_fees);
            vItem.status.setText(c.status);
            String mydate=Tools.getDate(c.date_ship);
            vItem.date_ship.setText("Waktu Kirim : "+mydate+", Pukul:"+c.time_ship);
            vItem.address.setText("Lokasi Kirim : "+c.address);
            vItem.payment.setText("Pembayaran : "+c.shipping);
            vItem.note.setText("Note : "+c.comment);
            List<ProductOrderDetail> pod = c.details;
            String _items="";
            double total_merchant=0;
            final Long _admin_id=5L;
            final String _admin="Admin";

            int i = 0;
            for(i = 0; i<pod.size(); i++){
                double total=(pod.get(i).amount) *(pod.get(i).price_item);
               /* if(userProfile.id.equals(pod.get(i).merchant_id+"")) {
                    _items+="<b>"+(i+1)+". "+pod.get(i).product_name+" ("+pod.get(i).amount+") = Rp."+(int)total+"</b>";

                }else{
                    _items+=(i+1)+". "+pod.get(i).product_name+" ("+pod.get(i).amount+") = Rp."+(int)total;
                }*/
                if(userProfile.is_admin==1) {
                    _items += (i + 1) + ". " + pod.get(i).product_name + " (x" + pod.get(i).amount + ") = Rp." + (int) total;
                    _items +="\n By " + pod.get(i).merchant_name  + " ("+ pod.get(i).merchant_phone + ")";
                }else{
                    _items += (i + 1) + ". " + pod.get(i).product_name + " By " + pod.get(i).merchant_name + "(x" + pod.get(i).amount + ") = Rp." + (int) total;
                }

                if(userProfile.id.equals(pod.get(i).merchant_id+"")){
                    //product is owner by user
                    total_merchant=total_merchant+total;
                    _items+="=========> ORDER KE KAMU";

                }
                _items+="\n";
            }
            _items+=(i+1)+". "+"Fee Delivery "+ "Rp."+c.fee_delivery;
            _items+="\n"+(i+1)+". "+"Infaq/Sodaqah "+ "Rp."+c.fee_infaq;

            //show price only current merchant
            if(userProfile.is_admin!=1) {
                vItem.price.setText(total_merchant + "");
            }

            vItem.details.setText("Items :\n\n"+_items);

            vItem.date.setText(Tools.getFormattedDateSimple(c.created_at));
            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.dialogUpdateStatus(c);
                }

            });

            //annonymous user
            /*if(c.user_id==0){
                vItem.send_message.setVisibility(View.GONE);
            }else {*/
                //btn msg & call
                vItem.send_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String msg="Kode Order : "+c.code;
                        msg+="\nTotal : "+c.total_fees;
                        ((ActivitySeller)ctx).openMessage(msg, Long.parseLong(c.user_id+""), c.buyer);
                        //Toast.makeText(ctx.getApplicationContext(), "test-pesan", Toast.LENGTH_LONG).show();

                    }
                });
            //}
            vItem.send_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ActivitySeller)ctx).openCall(c.phone);
                }
            });

            /*String _status=ctx.getResources().getString(R.string.selesai);
            Log.d("STATUS",_status);

            if(vItem.status.getText().equals(_status)) {
                vItem.status.setTextColor(ctx.getResources().getColor(R.color.colorPrice));
            }*/


            //button pay show
           /* if(c.status.equals(ctx.getResources().getString(R.string.proses_bayar))){
                vItem.lyt_action.setVisibility(View.VISIBLE);

                vItem.bt_payment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //fragment.dialogUpdateStatus(c);
                        Toast.makeText(ctx.getApplicationContext(), "test-payment", Toast.LENGTH_LONG).show();
                    }
                });

            }

                vItem.bt_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //fragment.dialogUpdateStatus(c);
                        String msg="Kode Order : "+c.code;
                         msg+="\nTotal : "+ c.total_fees;
                        ((ActivityBuyer)ctx).openMessage(msg, _admin_id, _admin);
                    }
                });

            vItem.bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   fragment.dialogUpdateStatus(c);

                }
            });*/

            } else {
               ((AdapterSellerOrder.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<ProductOrder> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }


    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }


    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / Constant.PRODUCT_PER_REQUEST;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

}