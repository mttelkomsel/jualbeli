package com.mtt.jualbeli.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.telephony.SmsManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.mtt.jualbeli.ActivityBuyer;
import com.mtt.jualbeli.ActivityWeb;
import com.mtt.jualbeli.broadcast.IncomingSms;
import com.mtt.jualbeli.data.Constant;

import im.delight.android.webview.AdvancedWebView;

import static com.thefinestartist.utils.content.ContextUtil.registerReceiver;

/**
 * Created by Qomarullah on 7/31/2017.
 */
public class WebViewJsInterface {

    private AdvancedWebView webView;
    private Context context;

    IncomingSms incomingSms;

    public WebViewJsInterface(Context context, AdvancedWebView webView) {
        this.webView = webView;
        this.context=context;
    }



    @JavascriptInterface
    public void handleMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
    @JavascriptInterface
    public void phoneCall(String phone) {
        //Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+ phone));
        context.startActivity(callIntent);
    }
    @JavascriptInterface
    public void closeActivity() {
        //Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        ActivityWeb activity = (ActivityWeb) context;
        ActivityBuyer.navigate(activity);
        activity.finish();
    }


    @JavascriptInterface
    public void phoneSMS(String tcash, String nominal) {
        ActivityWeb activity = (ActivityWeb) context;
        String sender = Constant.SMS_TCASH_SENDER;
        String msg = "TRF "+tcash+" "+nominal;
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(sender, null, msg, null, null);
            //Toast.makeText(activity, "SMS sukses, silakan coba kembali.", Toast.LENGTH_LONG).show();

            //start broadcast
            incomingSms = new IncomingSms();
            incomingSms.setMainActivityHandler(activity);
            registerReceiver(incomingSms, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

        }catch (Exception e) {
            //Toast.makeText(activity, "SMS gagal, silakan coba kembali.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }


    @JavascriptInterface
    public void setWebViewTextCallback(){
      /*  String script = WebViewUtils.formatScript("setText","This is a text from Android which is set " +
                "in the html page.");
        WebViewUtils.callJavaScriptFunction(webView,script);*/
    }
}
