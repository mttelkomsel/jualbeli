package com.mtt.jualbeli.utils;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.jualbeli.ActivitySellerProduct;
import com.mtt.jualbeli.R;
import com.mtt.jualbeli.adapter.AdapterCategory;
import com.mtt.jualbeli.adapter.AdapterSellerCategory;
import com.mtt.jualbeli.adapter.AdapterSellerProduct;
import com.mtt.jualbeli.model.Category;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;

public class DialogUtils {

    private Activity activity;

    public DialogUtils(Activity activity) {
        this.activity = activity;
    }

    private Dialog buildDialogView(@LayoutRes int layout) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        return dialog;
    }


    public Dialog buildDialogInfo(@StringRes int title, @StringRes int content, @StringRes int bt_text_pos, @DrawableRes int icon, final CallbackDialog callback) {
        return buildDialogInfo(activity.getString(title), activity.getString(content), activity.getString(bt_text_pos), icon, callback);
    }

    // dialog info
    public Dialog buildDialogInfo(String title, String content, String bt_text_pos, @DrawableRes int icon, final CallbackDialog callback) {
        final Dialog dialog = buildDialogView(R.layout.dialog_info);

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.content)).setText(content);
        ((Button) dialog.findViewById(R.id.bt_positive)).setText(bt_text_pos);
        ((ImageView) dialog.findViewById(R.id.icon)).setImageResource(icon);

        ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onPositiveClick(dialog);
            }
        });
        return dialog;
    }

    // dialog info
    public Dialog buildDialogSeek(int title, final int content, final int start, int bt_text_pos, int bt_text_neg, @DrawableRes int icon, final CallbackDialog callback) {
        final Dialog dialog = buildDialogView(R.layout.dialog_seek);
        SeekBar seekBar=(SeekBar) dialog.findViewById(R.id.seek_value);
        final TextView contentValue= (TextView) dialog.findViewById(R.id.content);
        //contentValue.setText(content);

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((Button) dialog.findViewById(R.id.bt_positive)).setText(bt_text_pos);
        ((Button) dialog.findViewById(R.id.bt_negative)).setText(bt_text_neg);
        ((ImageView) dialog.findViewById(R.id.icon)).setImageResource(icon);

        ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onPositiveClick(dialog);
            }
        });

        ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onNegativeClick(dialog);
            }
        });

        seekBar.setMax(50);
        seekBar.setProgress(start);
        contentValue.setText("Rp. "+(seekBar.getProgress()*1000));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = start;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                contentValue.setText("Rp. "+(progress*1000));
                //Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //textView.setText("Covered: " + progress + "/" + seekBar.getMax());
                //Toast.makeText(getApplicationContext(), "Stopped tracking seekbar", Toast.LENGTH_SHORT).show();
            }
        });
        return dialog;
    }
    public Dialog buildDialogCheckBox(final Activity act, int title, final ArrayList<Category> categories, int bt_text_pos, int bt_text_neg, @DrawableRes int icon, final CallbackDialog callback) {
        final Dialog dialog = buildDialogView(R.layout.dialog_checkbox);
        Log.d("CAT",categories.size()+"");

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((Button) dialog.findViewById(R.id.bt_positive)).setText(bt_text_pos);
        ((Button) dialog.findViewById(R.id.bt_negative)).setText(bt_text_neg);
        //((ImageView) dialog.findViewById(R.id.icon)).setImageResource(icon);

        ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onPositiveClick(dialog);
            }
        });

        ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onNegativeClick(dialog);
            }
        });
        /*final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        AdapterSellerCategory adapter = new AdapterSellerCategory(act, categories);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        adapter.setItems(categories);*/

        /*adapter.setOnItemClickListener(new AdapterSellerCategory.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Category obj) {
                //dialogOrderHistoryDetails(obj);
                CheckBox c= (CheckBox) view.findViewById(R.id.check);
                if(c.isChecked())
                    Toast.makeText(act, "unchecked:"+obj.name, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(act, "checked:"+obj.name, Toast.LENGTH_SHORT).show();

            }
        });*/

        ListView listView = (ListView) dialog.findViewById(R.id.list);
        List<String> values = new ArrayList<String>() ;
        for(int i=0;i<categories.size();i++){
            values.add(i, categories.get(i).id+". "+categories.get(i).name);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(act,
                R.layout.item_seller_category, R.id.name, values);
        listView.setAdapter(adapter);

        return dialog;
    }

    public Dialog buildDialogWarning(@StringRes int title, @StringRes int content, @StringRes int bt_text_pos, @StringRes int bt_text_neg, @DrawableRes int icon, final CallbackDialog callback) {
        String _title = null;
        String _content = null;
        String _bt_text_neg = null;

        if (title != -1) _title = activity.getString(title);
        if (content != -1) _content = activity.getString(content);
        if (bt_text_neg != -1) _bt_text_neg = activity.getString(bt_text_neg);

        return buildDialogWarning(_title, _content, activity.getString(bt_text_pos), _bt_text_neg, icon, callback);
    }

    public Dialog buildDialogWarning(@StringRes int title, @StringRes int content, @StringRes int bt_text_pos, @DrawableRes int icon, final CallbackDialog callback) {
        String _title = null;
        String _content = null;

        if (title != -1) _title = activity.getString(title);
        if (content != -1) _content = activity.getString(content);

        return buildDialogWarning(_title, _content, activity.getString(bt_text_pos), null, icon, callback);
    }

    // dialog warning
    public Dialog buildDialogWarning(String title, String content, String bt_text_pos, String bt_text_neg, @DrawableRes int icon, final CallbackDialog callback) {
        final Dialog dialog = buildDialogView(R.layout.dialog_warning);

        // if id = -1 view will gone
        if (title != null) {
            ((TextView) dialog.findViewById(R.id.title)).setText(title);
        } else {
            ((TextView) dialog.findViewById(R.id.title)).setVisibility(View.GONE);
        }

        // if id = -1 view will gone
        if (content != null) {
            ((TextView) dialog.findViewById(R.id.content)).setText(content);
        } else {
            ((TextView) dialog.findViewById(R.id.content)).setVisibility(View.GONE);
        }
        ((Button) dialog.findViewById(R.id.bt_positive)).setText(bt_text_pos);
        if (bt_text_neg != null) {
            ((Button) dialog.findViewById(R.id.bt_negative)).setText(bt_text_neg);
        } else {
            ((Button) dialog.findViewById(R.id.bt_negative)).setVisibility(View.GONE);
        }
        ((ImageView) dialog.findViewById(R.id.icon)).setImageResource(icon);

        ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onPositiveClick(dialog);
            }
        });

        ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onNegativeClick(dialog);
            }
        });
        return dialog;
    }

}
