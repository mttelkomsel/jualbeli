package com.mtt.jualbeli.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Product implements Serializable {

    public Long id;
    public String name;
    public String image;
    public Double price;
    public Double fee;

    public Long stock;
    public Integer draft;
    public String description;
    public String status;
    public String category_id;
    public Long created_at;
    public Long last_update;
    public Long merchant_id;
    public String username;
    public String address;
    public String phone;
    public int type;
   

    public List<Category> categories = new ArrayList<>();
    public List<ProductImage> product_images = new ArrayList<>();


}
