package com.mtt.jualbeli.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductOrder implements Serializable {

    public int id;
    public int user_id;
    public String code;
    public String buyer;
    public String address;
    public String email;
    public String shipping;
    public Long date_ship;
    public String time_ship;

    public String phone;
    public String comment;
    public String status;
    public double total_fees;
    public double tax;
    public double fee_delivery;
    public double fee_infaq;

    public Long created_at = System.currentTimeMillis();
    public Long last_update = System.currentTimeMillis();
    public List<ProductOrderDetail> details = new ArrayList<>();

    public ProductOrder() {
    }

    public void ProductOrder(BuyerProfile buyerProfile, String shipping, String address,  Long date_ship, String time_ship, String comment, double fee_delivery, double fee_infaq) {
        this.user_id = 0;
        this.buyer = buyerProfile.name;
        this.address = address;
        this.email = buyerProfile.email;
        this.phone = buyerProfile.phone;
        this.shipping = shipping;
        this.date_ship = date_ship;
        this.time_ship = time_ship;
        this.comment = comment;
        this.fee_delivery=fee_delivery;
        this.fee_infaq=fee_infaq;

    }
    public void ProductOrderByUser(UserProfile userProfile, String shipping, String address, Long date_ship, String time_ship, String comment, double fee_delivery, double fee_infaq) {
        this.user_id = Integer.parseInt(userProfile.id);
        this.buyer = userProfile.username;
        this.address = address;
        this.email = userProfile.email;
        this.phone = userProfile.phone;
        this.shipping = shipping;
        this.date_ship = date_ship;
        this.time_ship = time_ship;
        this.comment = comment;
        this.fee_delivery=fee_delivery;
        this.fee_infaq=fee_infaq;

    }
}
