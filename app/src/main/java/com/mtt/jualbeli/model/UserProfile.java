package com.mtt.jualbeli.model;

import java.io.Serializable;

public class UserProfile implements Serializable {

    public String id;
    public String username;
    public String phone;
    public String email;
    public String password;
    public int is_merchant;
    public String address;
    public String name;
    public int open;
    public String telegram;
    public String nik_tsel;
    public String email_tsel;
    public String location;
    public String google_id;
    public String google_email;
    public String fb_id;
    public String fb_email;
    public String last_update;
    public int is_admin;

 }
