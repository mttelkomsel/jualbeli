package com.mtt.jualbeli.model;

import java.io.Serializable;

/**
 * Created by Qomarullah on 07/01/16.
 */
public class ChatMessage implements Serializable {
    public Long id;
    public String from_id;
    public String to_id;
    public String msg;
    public String username;
    public String phone;
    public String email;
    // extra attribute
    public Boolean read = false;
    public Long created_at;
    public int status;


}
