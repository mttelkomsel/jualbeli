package com.mtt.jualbeli.model;

import java.io.Serializable;

public class ChatUser implements Serializable {
    public Long id;
    public String from_id;
    public String to_id;
    public String msg;
    public String username;
    public String phone;
    public String email;
    // extra attribute
    public Boolean read = false;
    public Long created_at;
    public int status;
}
