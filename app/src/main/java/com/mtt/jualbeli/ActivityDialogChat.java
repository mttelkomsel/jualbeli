package com.mtt.jualbeli;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mtt.jualbeli.data.DatabaseHandler;
import com.mtt.jualbeli.model.ChatUser;
import com.mtt.jualbeli.utils.Tools;

public class ActivityDialogChat extends AppCompatActivity {

    private static final String EXTRA_OBJECT = "key.EXTRA_OBJECT";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";
    private static final String EXTRA_POSITION = "key.EXTRA_FROM_POSITION";

    // activity transition
    public static void navigate(Activity activity, ChatUser obj, Boolean from_notif, int position) {
        Intent i = navigateBase(activity, obj, from_notif);
        i.putExtra(EXTRA_POSITION, position);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, ChatUser obj, Boolean from_notif) {
        Intent i = new Intent(context, ActivityDialogChat.class);
        i.putExtra(EXTRA_OBJECT, obj);
        i.putExtra(EXTRA_FROM_NOTIF, from_notif);
        return i;
    }

    private Boolean from_notif;
    private ChatUser chatUser;
    private Intent intent;
    private DatabaseHandler db;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_chat);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        db = new DatabaseHandler(this);

        chatUser = (ChatUser) getIntent().getSerializableExtra(EXTRA_OBJECT);
        from_notif = getIntent().getBooleanExtra(EXTRA_FROM_NOTIF, false);
        position = getIntent().getIntExtra(EXTRA_POSITION, -1);

        // set chatUser as read
        //chatUser.read = true;
        //db.saveChatUser(chatUser);

        initComponent();
    }

    private void initComponent() {
        ((TextView) findViewById(R.id.title)).setText(chatUser.username);
        ((TextView) findViewById(R.id.content)).setText(chatUser.msg);
        ((TextView) findViewById(R.id.date)).setText(Tools.getFormattedDate(chatUser.created_at));

        //from_notif=true;
        ((RelativeLayout) findViewById(R.id.lyt_image)).setVisibility(View.GONE);
        ((Button) findViewById(R.id.bt_delete)).setVisibility(View.GONE);

        intent = new Intent(this, ActivitySplash.class);
        intent = ActivityChatDetails.navigateBase(this, Long.parseLong(chatUser.from_id), from_notif, chatUser.username);

        ((ImageView) findViewById(R.id.img_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((Button) findViewById(R.id.bt_open)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(intent);
            }
        });

    }
}
