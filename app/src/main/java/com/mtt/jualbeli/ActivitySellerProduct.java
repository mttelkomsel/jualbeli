package com.mtt.jualbeli;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.balysv.materialripple.MaterialRippleLayout;
import com.mtt.jualbeli.adapter.AdapterNotification;
import com.mtt.jualbeli.adapter.AdapterSellerProduct;
import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackProduct;
import com.mtt.jualbeli.connection.callbacks.CallbackStatus;
import com.mtt.jualbeli.connection.callbacks.CallbackUser;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.DatabaseHandler;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.Category;
import com.mtt.jualbeli.model.Notification;
import com.mtt.jualbeli.model.Product;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.CallbackDialog;
import com.mtt.jualbeli.utils.DialogUtils;
import com.mtt.jualbeli.utils.FileUtils;
import com.mtt.jualbeli.utils.NetworkCheck;
import com.mtt.jualbeli.utils.Tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.MEDIA_MOUNTED;

public class ActivitySellerProduct extends AppCompatActivity {

    public View parent_view;
    private RecyclerView recyclerView;
    private DatabaseHandler db;
    public AdapterNotification adapter;
    static ActivitySellerProduct activityNotification;
    private SharedPref sharedPref;
    private EditText location;
    private TimePickerDialog timePickerDialog;

    //private Spinner location;

    private MaterialRippleLayout lyt_save_user;
    private MaterialRippleLayout lyt_save_merchant;

    private UserProfile userProfile;

    private Boolean edit=false;
    private Product product;


    Bitmap myBitmap;
    Uri picUri;

    private static final String EXTRA_IMAGE_ID = "key.EXTRA_IMAGE_ID";
    private static final String EXTRA_IMAGE_NAME = "key.EXTRA_IMAGE_NAME";
    private static final String EXTRA_OBJECT = "key.EXTRA_OBJECT";


    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 107;

    private ImageView img0,img1,img2;

    private EditText name, stock, price, description;
    private Spinner type;
    private Button bt_category;
    private TextView list_category, fee_merchant, text_img;
    private LinearLayout lyt_fee_merchant, lyt_save, lyt_fee, lyt_img;

    File getImage;
    private String nameFile="name.jpg";
    private String name0="0.jpg";
    private String name1="1.jpg";
    private String name2="2.jpg";

    private int img;
    public static ActivitySellerProduct getInstance() {
        return activityNotification;
    }

    // activity transition
    public static void navigate(Activity activity) {
        Intent i = new Intent(activity, ActivitySellerProduct.class);
        activity.startActivity(i);
    }

    public static void navigateBase(Activity activity, Product obj) {
        Intent i = new Intent(activity, ActivitySellerProduct.class);
        i.putExtra(EXTRA_OBJECT, obj);
        activity.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);

        userProfile = sharedPref.getInfoUser();

        setContentView(R.layout.activity_seller_product);
        //db = new DatabaseHandler(this);

        product = (Product) getIntent().getSerializableExtra(EXTRA_OBJECT);
        if(product!=null)edit=true;

        initToolbar();
        initComponent();


        if(edit){
            name.setText(product.name);
            stock.setText(product.stock+"");
            price.setText(product.price+"");
            description.setText(product.description);
            type.setVisibility(View.GONE);
            text_img.setVisibility(View.GONE);
            lyt_fee.setVisibility(View.GONE);
            lyt_img.setVisibility(View.GONE);

            /*img0.setVisibility(View.GONE);
            img1.setVisibility(View.GONE);
            img2.setVisibility(View.GONE);*/

            bt_category.setVisibility(View.GONE);
            list_category.setVisibility(View.GONE);
            lyt_fee_merchant.setVisibility(View.GONE);
            fee_merchant.setVisibility(View.GONE);
        }

    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);


        if(edit)
            actionBar.setTitle("Edit Product");
        else
            actionBar.setTitle("New Product");


        Tools.systemBarLolipop(this);
    }

    private void initComponent() {
        parent_view = findViewById(android.R.id.content);

        name0 = System.currentTimeMillis()+".jpg";
        name1 = System.currentTimeMillis()+".jpg";
        name2 = System.currentTimeMillis()+".jpg";


        Log.d("CAM","CAPTURE:"+name0+"|"+name1+"|"+name2);

        name = (EditText) findViewById(R.id.name);
        stock = (EditText) findViewById(R.id.stock);
        price = (EditText) findViewById(R.id.price);
        text_img = (TextView) findViewById(R.id.text1);

        price.setText("0");
        stock.setText("1");
        description = (EditText) findViewById(R.id.description);

        type = (Spinner) findViewById(R.id.type);
        type.setSelection(0);

        bt_category = (Button) findViewById (R.id.bt_category);
        list_category = (TextView) findViewById (R.id.list_category);

        lyt_fee = (LinearLayout) findViewById (R.id.lyt_fee);
        lyt_img = (LinearLayout) findViewById (R.id.lyt_img);

        lyt_fee_merchant = (LinearLayout) findViewById (R.id.lyt_fee_merchant);
        fee_merchant = (TextView) findViewById (R.id.fee_merchant);

        lyt_save = (LinearLayout) findViewById (R.id.lyt_save);

        bt_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPickCategory();
            }
        });

        lyt_fee_merchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPickFee();
            }
        });
        lyt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!edit) {

                    Product product = new Product();
                    String _name = name.getText().toString();
                    Double _price = Double.parseDouble(price.getText().toString());
                    Double _fee = Double.parseDouble(fee_merchant.getText().toString());
                    Long _stock = Long.parseLong(stock.getText().toString());
                    String _category = list_category.getText().toString();
                    int _type = type.getSelectedItemPosition();
                    String _image = name0.substring(name0.lastIndexOf("/") + 1, name0.length());
                    long _merchant_id = Long.parseLong(userProfile.id);
                    String _description = description.getText().toString();
                    Integer _draft = 0;
                    String _status = "READY STOCK";
                    Long _created_at = Tools.getTime();
                    Long _last_update = Tools.getTime();
                    boolean image = false;
                    File f0 = new File(name0);
                    if (f0.exists()) {
                        image = true;
                    }

                    if (!_name.equals("") && !_price.equals("") && !_stock.equals("")
                            && !_category.equals("") && !_description.equals("") && image) {

                        //upload primary
                        uploadFile(getUriImage(name0));

                        File f1 = new File(name1);
                        if (f1.exists()) {
                            uploadFile(getUriImage(name1));
                        }
                        File f2 = new File(name2);
                        if (f2.exists()) {
                            uploadFile(getUriImage(name2));
                        }

                        product.name = _name;
                        product.image = _image;
                        product.price = _price;
                        product.fee = _fee;
                        product.stock = _stock;
                        product.type = _type;
                        product.category_id = _category;
                        product.merchant_id = _merchant_id;
                        product.draft = _draft;
                        product.description = _description;
                        product.status = _status;
                        product.created_at = _created_at;
                        product.last_update = _last_update;
                        product.stock = Long.parseLong(stock.getText().toString());
                        insertProduct(product);

                    } else {
                        Toast.makeText(ActivitySellerProduct.this, "Submit product gagal, pastikan semua data terisi benar", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Product _product = new Product();
                    String _name = name.getText().toString();
                    Double _price = Double.parseDouble(price.getText().toString().trim());
                    Long _stock = Long.parseLong(stock.getText().toString());
                    String _description = description.getText().toString();
                    Integer _draft = 0;
                    String _status = "READY STOCK";
                    Long _created_at = Tools.getTime();
                    Long _last_update = Tools.getTime();

                    if (!_name.equals("") && !_price.equals("") && !_stock.equals("")
                            && !_description.equals("")) {

                        _product.id = product.id;
                        _product.name = _name;
                        _product.price = _price;
                        _product.stock = _stock;
                        _product.draft = _draft;
                        _product.description = _description;
                        _product.status = _status;
                        _product.created_at = _created_at;
                        _product.last_update = _last_update;
                        _product.stock = Long.parseLong(stock.getText().toString());
                        updateProduct(_product);
                    } else {
                        Toast.makeText(ActivitySellerProduct.this, "Update product gagal, pastikan semua data terisi benar", Toast.LENGTH_SHORT).show();
                    }

                }//end else edit
            }
        });

        List<String> type_list = new ArrayList<>();
        //shipping_list.add(getString(R.string.choose_shipping));
        type_list.add(0,"Pembayaran via MTT");
        type_list.add(1,"Delivery & Pembayaran via MTT");
        type_list.add(2,"Product Iklan");
        //type_list.add(3,"Support Product Digital");

        // Initialize and set Adapter
        ArrayAdapter adapter_type = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, type_list.toArray());
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(adapter_type);




        img0 = (ImageView) findViewById(R.id.img0);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img=0;
                startActivityForResult(getPickImageChooserIntent(0,name0), 200);
            }
        });
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img=1;
                startActivityForResult(getPickImageChooserIntent(1,name1), 200);
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img=2;

                startActivityForResult(getPickImageChooserIntent(2,name2), 200);
            }
        });

    }

    public void dialogPickFee() {
        Dialog dialog = new DialogUtils(this).buildDialogSeek(R.string.title_fee_merchant, R.string.title_fee, 1, R.string.OK, R.string.CLOSE, R.drawable.img_checkout_success, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                SeekBar seekBar=(SeekBar) dialog.findViewById(R.id.seek_value);
                double fee=seekBar.getProgress()*1000;
                fee_merchant.setText(fee+"");

            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dialogPickCategory() {


        final ArrayList<Category> categories = ActivityMain.getInstance().getCategoryArrayList();

        Dialog dialog = new DialogUtils(this).buildDialogCheckBox(this, R.string.title_category, categories, R.string.OK, R.string.CLOSE, R.drawable.img_checkout_success, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                //final ListView listView = (ListView) dialog.findViewById(R.id.list);
                EditText input_category = (EditText) dialog.findViewById(R.id.input_category);
                list_category.setText(input_category.getText().toString());
            }


            @Override
            public void onNegativeClick(Dialog dialog) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.menu_activity_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    /////////////////////////////////
    public Intent getPickImageChooserIntent(final int id, final String nameFile) {

        Log.d("CHOOSE",id+"="+nameFile);
        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri(nameFile);

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
       /* Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
             }
            allIntents.add(intent);
        }
*/
        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }


        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }


        allIntents.remove(mainIntent);
        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        chooserIntent.putExtra(EXTRA_IMAGE_ID, id);
        chooserIntent.putExtra(EXTRA_IMAGE_NAME, nameFile);

        return chooserIntent;
    }


    /**
     * Get URI to image received from capture by camera.
     */


    private Uri getCaptureImageOutputUri(String name) {
        Uri outputFileUri = null;
        getImage = getExternalCacheDir();

        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), name));
        }
        return outputFileUri;
    }
    private Uri getUriImage(String path) {
         Uri outputFileUri = null;
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(path));
        }
        return outputFileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bitmap bitmap;
        if (resultCode == Activity.RESULT_OK) {

            ImageView imageView = (ImageView) findViewById(R.id.img0);
            if (img == 0) {
                nameFile=name0;
                imageView = (ImageView) findViewById(R.id.img0);
            }
            if (img == 1) {
                nameFile=name1;
                imageView = (ImageView) findViewById(R.id.img1);
            }
            if (img == 2) {
                nameFile=name2;
                imageView = (ImageView) findViewById(R.id.img2);
            }


                /////////////////ENDSETUP/////////////////////

                if (getPickImageResultUri(data, nameFile) != null) {
                    picUri = getPickImageResultUri(data, nameFile);
                    /*boolean isCamera=true;
                    if (data != null) {
                        String action = data.getAction();
                        isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                    }*/
                    //from image gallery
                   // if(!isCamera){
                        Uri selectedImageURI = data.getData();
                        nameFile = getRealPathFromURI(selectedImageURI);
                        if (img == 0)name0=nameFile;
                        if (img == 1)name1=nameFile;
                        if (img == 2)name2=nameFile;
                        picUri = selectedImageURI;
                    /*} else{
                        if (getImage == null) {
                            getImage = getExternalCacheDir();
                        }

                        if (img == 0){
                            name0 = getImage.getPath()+"/"+name0;
                            picUri=getUriImage(name0);
                        }
                        if (img == 1){
                            name1 = getImage.getPath()+"/"+name1;
                            picUri=getUriImage(name1);
                        }
                        if (img == 2){
                            name2 = getImage.getPath()+"/"+name2;
                            picUri=getUriImage(name2);
                        }

                    }
*/
                    Log.d("Path", name0+"|"+name1+"|"+name2);
                    Log.d("Path", img+"=>"+picUri);

                    try {
                        myBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picUri);
                        myBitmap = rotateImageIfRequired(myBitmap, picUri);
                        myBitmap = getResizedBitmap(myBitmap, 500);

                        //CircleImageView croppedImageView = (CircleImageView) findViewById(R.id.img_profile);
                        //croppedImageView.setImageBitmap(myBitmap);
                        imageView.setImageBitmap(myBitmap);
                        //Log.d("CAM","SINI1");

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.d("CAM","SINI-error");

                    }
                    Log.d("CAM","SINI2");


                } else {

                    Log.d("CAM","SINI3");

                    bitmap = (Bitmap) data.getExtras().get("data");
                    myBitmap = bitmap;
                    imageView.setImageBitmap(myBitmap);

                }

        }

    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    /**
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data, String nameFile) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        if(isCamera){
            return getCaptureImageOutputUri(nameFile);
        }else{
            return data.getData();
        }
       // return isCamera ? getCaptureImageOutputUri(nameFile) : data.getData();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        //outState.putParcelable("pic_uri", picUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        //picUri = savedInstanceState.getParcelable("pic_uri");
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (hasPermission(perms)) {

                    } else {

                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                //Log.d("API123", "permisionrejected " + permissionsRejected.size());

                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void insertProduct(Product product) {
        API api = RestAdapter.createAPI();
        Call<CallbackProduct> callbackCall = api.insertProduct(product);
        callbackCall.enqueue(new Callback<CallbackProduct>() {
            @Override
            public void onResponse(Call<CallbackProduct> call, Response<CallbackProduct> response) {
                CallbackProduct resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    //showProgress(false);
                    Toast.makeText(ActivitySellerProduct.this,  "Insert product sukses", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    Toast.makeText(ActivitySellerProduct.this, "Insert product gagal", Toast.LENGTH_SHORT).show();
                    return;
                }

            }

            @Override
            public void onFailure(Call<CallbackProduct> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                //showProgress(false);

            }
        });
    }


    private void updateProduct(Product product) {
        API api = RestAdapter.createAPI();
        Call<CallbackProduct> callbackCall = api.updateProduct(product.id, product.name, product.price,
                product.stock,product.description);
        callbackCall.enqueue(new Callback<CallbackProduct>() {
            @Override
            public void onResponse(Call<CallbackProduct> call, Response<CallbackProduct> response) {
                CallbackProduct resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    //showProgress(false);
                    Toast.makeText(ActivitySellerProduct.this,  "Update product sukses", Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    Toast.makeText(ActivitySellerProduct.this,  "Update product gagal", Toast.LENGTH_SHORT).show();
                    return;
                }

            }

            @Override
            public void onFailure(Call<CallbackProduct> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                //showProgress(false);

            }
        });
    }

    private void uploadFile(Uri fileUri) {
        API api = RestAdapter.createAPI();

        File file = FileUtils.getFile(this, fileUri);
        RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
        RequestBody filename  = RequestBody.create(MediaType.parse("text/plain"), file.getName());


        // finally, execute the request
        Call<CallbackStatus> call = api.upload(body, filename);
        call.enqueue(new Callback<CallbackStatus>() {
            @Override
            public void onResponse(Call<CallbackStatus> call, Response<CallbackStatus> response) {

               /* Log.v("Upload", "success:"+response.raw().body().toString());
                Toast.makeText(ActivitySellerProduct.this,  "Upload Image sukses", Toast.LENGTH_SHORT).show();
*/
                CallbackStatus resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    Log.d("RESP:",resp.value);
                    //showProgress(false);
                    Toast.makeText(ActivitySellerProduct.this,  "Upload image success", Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    Log.d("RESP:",resp.value);
                    //showProgress(false);
                    Toast.makeText(ActivitySellerProduct.this,  "Upload image failed", Toast.LENGTH_SHORT).show();
                    return;


                }

            }

            @Override
            public void onFailure(Call<CallbackStatus> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                Toast.makeText(ActivitySellerProduct.this,  "Upload image gagal", Toast.LENGTH_SHORT).show();

            }
        });
    }


    public String getMimeType(Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = this.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }
}
