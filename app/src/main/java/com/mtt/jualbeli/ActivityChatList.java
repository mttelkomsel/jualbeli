package com.mtt.jualbeli;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mtt.jualbeli.adapter.AdapterChatList;
import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackChatList;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.DatabaseHandler;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.ChatUser;
import com.mtt.jualbeli.utils.NetworkCheck;
import com.mtt.jualbeli.utils.Tools;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChatList extends AppCompatActivity {

    private View parent_view;
    private RecyclerView recyclerView;
    private AdapterChatList mAdapter;
    private SwipeRefreshLayout swipe_refresh;
    private Call<CallbackChatList> callbackCall = null;

    private int post_total = 0;
    private int failed_page = 0;

    private SharedPref sharedPref;

    private String userId;
    private String from;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private DatabaseHandler db;
    private Intent intent;

    // activity transition
    public static void navigate(Activity activity) {
        Intent i = new Intent(activity, ActivityChatList.class);
        activity.startActivity(i);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        sharedPref = new SharedPref(this);

        //cek login in mainactivity
        userId = sharedPref.getInfoUser().id;

        db = new DatabaseHandler(this);

        parent_view = findViewById(android.R.id.content);
        initToolbar();
        iniComponent();
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_chat);
        Tools.systemBarLolipop(this);
    }

    public void iniComponent() {
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //set data and list adapter
        mAdapter = new AdapterChatList(this, recyclerView, new ArrayList<ChatUser>());
        recyclerView.setAdapter(mAdapter);

        intent = new Intent(this, ActivitySplash.class);
        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterChatList.OnItemClickListener() {
            @Override
            public void onItemClick(View v, ChatUser obj, int position) {
                obj.read = true;
                intent = ActivityChatDetails.navigateBase(ActivityChatList.this, Long.parseLong(obj.from_id), false, obj.username);
                startActivity(intent);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterChatList.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (post_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });

        requestAction(1);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)) {
                    // new push message is received
                    handlePushNotification(intent);
                }
            }
        };

    }

    private void displayApiResult(final List<ChatUser> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) {
            showNoItemView(true);
        }else
            setCounterUnread(items);

    }
    private void setCounterUnread(List<ChatUser> items){
        int unread=0;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i) == null) {
                if(items.get(i).status<2){
                    unread++;
                }
            }
        }
        //update
        sharedPref.setChatUnread(unread);
        ActivityMain.getInstance().updateNavCounter();
    }

    private void requestChatList(final int page_no) {
        API api = RestAdapter.createAPI();
        callbackCall = api.getChat(page_no, Constant.PRODUCT_PER_REQUEST, userId);
        callbackCall.enqueue(new Callback<CallbackChatList>() {
            @Override
            public void onResponse(Call<CallbackChatList> call, Response<CallbackChatList> response) {
                CallbackChatList resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    post_total = resp.count_total;
                    displayApiResult(resp.chat_user);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<CallbackChatList> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "");
        showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestChatList(page_no);
            }
        }, 2000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) findViewById(R.id.lyt_no_item);
        ((TextView) findViewById(R.id.no_item_message)).setText(R.string.no_item);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {

        // registering the receiver for new notification
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constant.PUSH_NOTIFICATION));

        Tools.clearNotifications();

        mAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
    /**
     * Handling new push message, will add the message to
     * recycler view and scroll it to bottom
     * */
    private void handlePushNotification(Intent intent) {
        //TODO how notify without reload
        mAdapter.resetListData();
        requestAction(1);

    }
}
