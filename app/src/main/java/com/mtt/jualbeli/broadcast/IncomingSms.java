package com.mtt.jualbeli.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.mtt.jualbeli.ActivityWeb;
import com.mtt.jualbeli.data.Constant;


/**
 * Created by Qomarullah on 8/15/2016.
 */
public class IncomingSms extends BroadcastReceiver {

    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();
    static String TAG="INCOMING-SMS";

    ActivityWeb main = null;
    public void setMainActivityHandler(ActivityWeb main){
        this.main=main;
    }

    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    // if the SMS is not from our gateway, ignore the message
                    /*if (!senderNum.toLowerCase().contains(Constant.SMS_TCASH_SENDER.toLowerCase())) {
                        Log.e(TAG, "SMS is not for our app!");
                        return;
                    }*/
                if (!senderNum.equals(Constant.SMS_TCASH_STATUS)) {
                        Log.e(TAG, "SMS is not for our app!");
                        return;
                    }
                    //get confirmation message
                  if(message.indexOf("No Ref:")!=-1) {
                        //sukses
                        String desc = message.substring(message.indexOf("No Ref:"), message.indexOf("Info"));

                        main.finishSms(true,desc);

                    }else{
                        //error or other
                        main.finishSms(false, message);

                    }
                   /* Intent hhtpIntent = new Intent(context, HttpService.class);
                    hhtpIntent.putExtra("otp", verificationCode);
                    context.startService(hhtpIntent);*/

                   /* Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);
                    // Show Alert
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context,
                            "senderNum: "+ senderNum + ", message: " + message, duration);
                    toast.show();*/

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}