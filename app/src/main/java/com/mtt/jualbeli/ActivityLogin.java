package com.mtt.jualbeli;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackUser;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.ChatUser;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.CallbackDialog;
import com.mtt.jualbeli.utils.DialogUtils;
import com.mtt.jualbeli.utils.NetworkCheck;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Android login screen Activity
 * http://javapapers.com/android/beautiful-android-login-screen-design-tutorial/
 */

public class ActivityLogin extends AppCompatActivity {

    private View loginFormView;
    private View progressView;
    private AutoCompleteTextView emailTextView;
    private EditText passwordTextView;
    private TextView signUpText;
    private TextView resetText ;

    private UserProfile userProfile;
    private SharedPref sharedPref;
    private SwipeRefreshLayout swipe_refresh;
    private View parent_view;

    private String email, password;
    private Boolean toProfile=false;

    private static final String EXTRA_TO_PROFILE = "key.EXTRA_TO_PROFILE";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);

        toProfile = getIntent().getBooleanExtra(EXTRA_TO_PROFILE, false);

        //display must login
        Toast.makeText(getApplicationContext(), getString(R.string.must_login), Toast.LENGTH_LONG).show();

        setContentView(R.layout.activity_login);
        parent_view = findViewById(android.R.id.content);

        emailTextView = (AutoCompleteTextView) findViewById(R.id.email);
        //loadAutoComplete();
        passwordTextView = (EditText) findViewById(R.id.password);
        passwordTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    initLogin();
                    return true;
                }
                return false;
            }
        });

        Button loginButton = (Button) findViewById(R.id.email_sign_in_button);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                initLogin();
            }
        });

        loginFormView = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);

        //adding underline and link to signup textview
        signUpText = (TextView) findViewById(R.id.signUpText);
        signUpText.setPaintFlags(signUpText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Linkify.addLinks(signUpText, Linkify.ALL);

        signUpText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("LoginActivity", "Sign Up Activity activated.");
                // this is where you should start the signup Activity
                // LoginActivity.this.startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                Intent i = new Intent(ActivityLogin.this, ActivitySignUp.class);
                startActivity(i);
                finish();

            }
        });

        resetText = (TextView) findViewById(R.id.resetText);
        resetText.setPaintFlags(resetText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Linkify.addLinks(resetText, Linkify.ALL);

        resetText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("LoginActivity", "Sign Up Activity activated.");
                // this is where you should start the signup Activity
                // LoginActivity.this.startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                Intent i = new Intent(ActivityLogin.this, ActivityReset.class);
                startActivity(i);
                finish();

            }
        });


    }

    // activity transition
    public static void navigate(Activity activity) {
        Intent i = navigateBase(activity, false);
        activity.startActivity(i);
    }

    // activity transition
    public static void navigate(Activity activity, Boolean toProfile) {
        Intent i = navigateBase(activity, toProfile);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, Boolean toProfile) {
        Intent i = new Intent(context, ActivityLogin.class);
        i.putExtra(EXTRA_TO_PROFILE, toProfile);
        return i;
    }


    /**
     * Validate Login form and authenticate.
     */
    public void initLogin() {


        emailTextView.setError(null);
        passwordTextView.setError(null);

        email = emailTextView.getText().toString();
        password = passwordTextView.getText().toString();

        boolean cancelLogin = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            passwordTextView.setError(getString(R.string.invalid_password));
            focusView = passwordTextView;
            cancelLogin = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailTextView.setError(getString(R.string.field_required));
            focusView = emailTextView;
            cancelLogin = true;
        } /*else if (!isEmailValid(email)) {
            emailTextView.setError(getString(R.string.invalid_email));
            focusView = emailTextView;
            cancelLogin = true;
        }*/

        if (cancelLogin) {
            // error in login
            focusView.requestFocus();
        } else {
            showProgress(true);
            startProcess(email, password);

        }
    }

    private boolean isEmailValid(String email) {
        //add your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //add your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }



    private void startProcess(String username,String password) {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            requestUser(username, password);
        }
    }

    private void requestUser(final String username, final String password ) {
        API api = RestAdapter.createAPI();
        Call<CallbackUser> callbackCall = api.getUser(username, password);
        callbackCall.enqueue(new Callback<CallbackUser>() {
            @Override
            public void onResponse(Call<CallbackUser> call, Response<CallbackUser> response) {
                CallbackUser resp = response.body();
                if (resp != null && resp.status.equals("success") && resp.user!= null) {
                    UserProfile userProfile = sharedPref.setInfoUser(resp.user);

                    showProgress(false);
                    //update fcm with userid
                    ThisApplication.getInstance().obtainFirebaseToken();

                    if(toProfile) {
                        Intent i = new Intent(ActivityLogin.this, ActivityProfile.class);
                        startActivity(i);
                    }else {
                        finish();
                    }

                }else if (resp != null && resp.status.equals("failed") && resp.user!= null) {
                    showProgress(false);
                    //Snackbar.make(parent_view, R.string.incorrect_password, Snackbar.LENGTH_SHORT).show();
                    Toast.makeText(ActivityLogin.this,  R.string.incorrect_password, Toast.LENGTH_SHORT).show();

                    return;
                }else {
                    showProgress(false);
                    //Snackbar.make(parent_view, R.string.invalid_login , Snackbar.LENGTH_SHORT).show();
                    Toast.makeText(ActivityLogin.this,  R.string.invalid_login, Toast.LENGTH_SHORT).show();

                    return;
                }

            }

            @Override
            public void onFailure(Call<CallbackUser> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                dialogServerNotConnect();
                showProgress(false);

            }
        });
    }


    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    public void dialogServerNotConnect() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_unable_connect, R.string.msg_unable_connect, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_connect, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    // make a delay to start next activity
    private void retryOpenApplication() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestUser(email, password);
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
