package com.mtt.jualbeli;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mtt.jualbeli.broadcast.IncomingSms;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.utils.Tools;
import com.mtt.jualbeli.utils.WebViewJsInterface;

import im.delight.android.webview.AdvancedWebView;

public class ActivityWeb extends AppCompatActivity implements AdvancedWebView.Listener {



    private AdvancedWebView mWebView;
    private static final String EXTRA_WEB_URL = "key.EXTRA_WEB_URL";
    private static final String EXTRA_WEB_TITLE = "key.EXTRA_WEB_TITLE";


    private ProgressBar progress;
    private String url, title;
    IncomingSms incomingSms;

    // activity transition
    public static void navigate(Activity activity, String url, String title) {
        Intent i = new Intent(activity, ActivityWeb.class);
        i.putExtra(EXTRA_WEB_URL, url);
        i.putExtra(EXTRA_WEB_TITLE, title);
        activity.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web);
        //setup url from caller
        url = getIntent().getStringExtra(EXTRA_WEB_URL);
        title = getIntent().getStringExtra(EXTRA_WEB_TITLE);

        initToolbar();
        //mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setListener(this, this);
        mWebView.setGeolocationEnabled(false);
        mWebView.setMixedContentAllowed(true);
        mWebView.setCookiesEnabled(true);
        mWebView.setThirdPartyCookiesEnabled(true);


        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setMax(100);

        mWebView.setWebChromeClient(new WebChromeClient(){

            public void onProgressChanged(WebView view, int progress) {
                ActivityWeb.this.setProgress(progress * 100);

            }
        });

        mWebView.getSettings().setJavaScriptEnabled(true);
        //Inject WebAppInterface methods into Web page by having Interface name 'Android'
        mWebView.addJavascriptInterface(new WebViewJsInterface(this, mWebView), "handler");

        mWebView.loadUrl(url);


    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(title);
        Tools.systemBarLolipop(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_activity_merchant, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);

        // ...
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) { return; }
        // ...
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        //mSwipeRefreshLayout.setRefreshing(true);
        progress.setProgress(0);
        progress.setVisibility(View.VISIBLE);

    }


    @Override
    public void onPageFinished(String url) {
        //mSwipeRefreshLayout.setRefreshing(false);
        progress.setProgress(100);
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) { }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) { }

    @Override
    public void onExternalPageRequest(String url) { }


    //Class to be injected in Web page
    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /**
         * Show Toast Message
         * @param toast
         */
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }

        public void openWebPage(String url) {
            Uri webpage = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }

    }
    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void finishSms(boolean status, String desc) {

        if (status) {
            unregisterReceiver(incomingSms);
            Toast.makeText(getApplicationContext(), "Transaksi berhasil, silakan kembali untuk konfirmasi dengan Admin " + desc, Toast.LENGTH_LONG).show();

        } else {
            unregisterReceiver(incomingSms);
            Toast.makeText(getApplicationContext(), "Transaksi Gagal, Silakan coba kembali" + desc, Toast.LENGTH_SHORT).show();
        }
    }
}
