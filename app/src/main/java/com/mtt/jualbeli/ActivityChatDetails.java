package com.mtt.jualbeli;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.jualbeli.adapter.AdapterChatDetail;
import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackChatDetails;
import com.mtt.jualbeli.connection.callbacks.CallbackChatSubmit;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.DatabaseHandler;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.ChatMessage;
import com.mtt.jualbeli.model.NewsInfo;
import com.mtt.jualbeli.utils.NetworkCheck;
import com.mtt.jualbeli.utils.Tools;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChatDetails extends AppCompatActivity {

    private View parent_view;
    private RecyclerView recyclerView;
    private AdapterChatDetail mAdapter;
    private SwipeRefreshLayout swipe_refresh;
    private Call<CallbackChatDetails> callbackCall = null;
    private Call<CallbackChatSubmit> callbackCallChat = null;

    private int post_total = 0;
    private int failed_page = 0;
    private String TAG = ActivityChatDetails.class.getSimpleName();

    static ActivityChatDetails activityNotification;
    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";
    private static final String EXTRA_TITLE = "key.EXTRA_TITLE";
    private static final String EXTRA_MSG = "key.EXTRA_MSG";

    private SharedPref sharedPref;

    private String userId;
    private String from;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private DatabaseHandler db;
    private String title;
    private String msg;


    private EditText inputMessage;
    private Button btnSend;


    public static ActivityChatDetails getInstance() {
        return activityNotification;
    }

    // activity transition
    public static void navigate(Activity activity, Long id, Boolean from_notif, String title) {
        Intent i = navigateBase(activity, id, from_notif, title);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, Long id, Boolean from_notif, String title) {
        Intent i = new Intent(context, ActivityChatDetails.class);
        i.putExtra(EXTRA_OBJECT_ID, id);
        i.putExtra(EXTRA_FROM_NOTIF, from_notif);
        i.putExtra(EXTRA_TITLE, title);

        return i;
    }
    public static Intent navigateBase(Context context, Long id, Boolean from_notif, String title, String msg) {
        Intent i = new Intent(context, ActivityChatDetails.class);
        i.putExtra(EXTRA_OBJECT_ID, id);
        i.putExtra(EXTRA_FROM_NOTIF, from_notif);
        i.putExtra(EXTRA_TITLE, title);
        i.putExtra(EXTRA_MSG, msg);

        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);

        setContentView(R.layout.activity_chat_details);
        parent_view = findViewById(android.R.id.content);

        //cek login
        if(sharedPref.isInfoUser()) {
            userId = sharedPref.getInfoUser().id;
        }else{
            Intent i = new Intent(ActivityChatDetails.this, ActivityLogin.class);
            startActivity(i);
            finish();
        }

        db = new DatabaseHandler(this);

        //get from
        from= getIntent().getLongExtra(EXTRA_OBJECT_ID, 0L)+"";
        title= getIntent().getStringExtra(EXTRA_TITLE);
        msg= getIntent().getStringExtra(EXTRA_MSG);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)) {
                    // new push message is received
                    handlePushNotification(intent);
                }
            }
        };

        initToolbar();
        iniComponent();
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(title);
        Tools.systemBarLolipop(this);
    }

    public void iniComponent() {
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //get userId
        final String userId=sharedPref.getInfoUser().id;

        //set data and list adapter
        mAdapter = new AdapterChatDetail(this, recyclerView, new ArrayList<ChatMessage>(), userId);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterChatDetail.OnItemClickListener() {
            @Override
            public void onItemClick(View v, NewsInfo obj, int position) {
                //ActivityNewsInfoDetails.navigate(Activityo.this, obj.id, false);

            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterChatDetail.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (post_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page, userId, from);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1, userId, from);
            }
        });

        // send message chat
        inputMessage = (EditText) findViewById(R.id.message);
        btnSend = (Button) findViewById(R.id.btn_send);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        //direct message from other activityOrderHistory
        if(msg!=null)sendMessage(msg);

        requestAction(1, userId, from);
    }

    /**
     * Handling new push message, will add the message to
     * recycler view and scroll it to bottom
     * */
    private void handlePushNotification(Intent intent) {
        ChatMessage message =  new ChatMessage();
        message.msg=intent.getStringExtra("msg");
        message.from_id=intent.getStringExtra("from_id");
        message.created_at=intent.getLongExtra("created_at",0L);


        if (message != null && from != null) {
            mAdapter.insertDataOne(message);
            mAdapter.notifyDataSetChanged();
            if (mAdapter.getItemCount() > 1) {
                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);
            }
        }
    }


    private void displayApiResult(final List<ChatMessage> items) {
        mAdapter.insertData(items);
        Log.e("Chat","total:"+items.size());
        swipeProgress(false);
        if (items.size() == 0) {
            showNoItemView(true);
        }else{
           recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);
        }
    }

    private void requestChatInfo(final int page_no, final String userId, final String from) {
        API api = RestAdapter.createAPI();
        callbackCall = api.getAllChatUser(page_no, Constant.NEWS_PER_REQUEST, userId, from);
        callbackCall.enqueue(new Callback<CallbackChatDetails>() {
            @Override
            public void onResponse(Call<CallbackChatDetails> call, Response<CallbackChatDetails> response) {
                CallbackChatDetails resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    post_total = resp.count_total;
                    displayApiResult(resp.chat);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<CallbackChatDetails> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction(final int page_no, final String userId, final String from) {
        showFailedView(false, "");
        showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestChatInfo(page_no, userId, from);
            }
        }, 2000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page, userId, from);
            }
        });
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) findViewById(R.id.lyt_no_item);
        ((TextView) findViewById(R.id.no_item_message)).setText("no_post");
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();

        // registering the receiver for new notification
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constant.PUSH_NOTIFICATION));

        Tools.clearNotifications();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void sendMessage(String msg){
        this.inputMessage.setText(msg);
        sendMessage();

    }
    private void sendMessage() {
        final String message = this.inputMessage.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            Toast.makeText(getApplicationContext(), "Enter a message", Toast.LENGTH_SHORT).show();
            return;
        }
        this.inputMessage.setText("");

        API api = RestAdapter.createAPI();
        callbackCallChat = api.submitChat(userId, from, message);
        callbackCallChat.enqueue(new Callback<CallbackChatSubmit>() {
            @Override
            public void onResponse(Call<CallbackChatSubmit> call, Response<CallbackChatSubmit> response) {
                CallbackChatSubmit resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    //displayApiResult(resp.chat);
                    mAdapter.insertDataOne(resp.data);
                    mAdapter.notifyDataSetChanged();
                    if (mAdapter.getItemCount() > 1) {
                        recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() - 1);
                    }
                } else {
                    //onFailRequest(page_no);
                    Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CallbackChatSubmit> call, Throwable t) {
                //if (!call.isCanceled()) onFailRequest(page_no);
                Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_SHORT).show();


            }

        });
    }

}
