package com.mtt.jualbeli.connection.callbacks;

import com.mtt.jualbeli.model.ChatMessage;
import com.mtt.jualbeli.model.NewsInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CallbackChatDetails implements Serializable {

    public String status = "";
    public int count = -1;
    public int count_total = -1;
    public int pages = -1;
    public List<ChatMessage> chat = new ArrayList<>();

}
