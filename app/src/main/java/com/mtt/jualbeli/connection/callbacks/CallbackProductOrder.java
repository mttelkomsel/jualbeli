package com.mtt.jualbeli.connection.callbacks;

import com.mtt.jualbeli.model.Product;
import com.mtt.jualbeli.model.ProductOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CallbackProductOrder implements Serializable {

    public String status = "";
    public int count = -1;
    public int count_total = -1;
    public int pages = -1;
    public List<ProductOrder> orders = new ArrayList<>();

}
