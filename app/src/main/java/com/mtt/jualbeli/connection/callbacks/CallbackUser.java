package com.mtt.jualbeli.connection.callbacks;

import com.mtt.jualbeli.model.Info;
import com.mtt.jualbeli.model.UserProfile;

import java.io.Serializable;

public class CallbackUser implements Serializable {
    public String status = "";
    public UserProfile user = null;
}
