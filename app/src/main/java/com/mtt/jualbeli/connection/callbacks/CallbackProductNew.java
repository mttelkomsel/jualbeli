package com.mtt.jualbeli.connection.callbacks;

import com.mtt.jualbeli.model.NewsInfo;
import com.mtt.jualbeli.model.Product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CallbackProductNew implements Serializable {

    public String status = "";
    public List<Product> products = new ArrayList<>();

}
