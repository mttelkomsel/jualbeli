package com.mtt.jualbeli.connection.callbacks;

import com.mtt.jualbeli.model.ChatMessage;

import java.io.Serializable;

public class CallbackChatSubmit implements Serializable {
    public String status = "";
    public String msg = "";
    public ChatMessage data = null;

}
