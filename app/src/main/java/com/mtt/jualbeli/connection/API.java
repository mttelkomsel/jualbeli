package com.mtt.jualbeli.connection;

import com.mtt.jualbeli.connection.callbacks.CallbackCategory;
import com.mtt.jualbeli.connection.callbacks.CallbackChatDetails;
import com.mtt.jualbeli.connection.callbacks.CallbackChatList;
import com.mtt.jualbeli.connection.callbacks.CallbackChatSubmit;
import com.mtt.jualbeli.connection.callbacks.CallbackDevice;
import com.mtt.jualbeli.connection.callbacks.CallbackFeaturedNews;
import com.mtt.jualbeli.connection.callbacks.CallbackInfo;
import com.mtt.jualbeli.connection.callbacks.CallbackNewsInfo;
import com.mtt.jualbeli.connection.callbacks.CallbackNewsInfoDetails;
import com.mtt.jualbeli.connection.callbacks.CallbackOrder;
import com.mtt.jualbeli.connection.callbacks.CallbackProduct;
import com.mtt.jualbeli.connection.callbacks.CallbackProductDetails;
import com.mtt.jualbeli.connection.callbacks.CallbackProductNew;
import com.mtt.jualbeli.connection.callbacks.CallbackProductOrder;
import com.mtt.jualbeli.connection.callbacks.CallbackStatus;
import com.mtt.jualbeli.connection.callbacks.CallbackUser;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.model.Checkout;
import com.mtt.jualbeli.model.DeviceInfo;
import com.mtt.jualbeli.model.Product;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface API {

    String CACHE = "Cache-Control: max-age=0";
    String AGENT = "User-Agent: JualBeli";
    String SECURITY = "Security: " + Constant.SECURITY_CODE;

    /* Recipe API transaction ------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("services/info")
    Call<CallbackInfo> getInfo(
            @Query("version") int version
    );

    /* Fcm API ----------------------------------------------------------- */
    @Headers({CACHE, AGENT, SECURITY})
    @POST("services/insertOneFcm")
    Call<CallbackDevice> registerDevice(
            @Body DeviceInfo deviceInfo
    );

    /* News Info API ---------------------------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("services/listFeaturedNews")
    Call<CallbackFeaturedNews> getFeaturedNews();

    @Headers({CACHE, AGENT})
    @GET("services/listNews")
    Call<CallbackNewsInfo> getListNewsInfo(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query
    );

    @Headers({CACHE, AGENT})
    @GET("services/getNewsDetails")
    Call<CallbackNewsInfoDetails> getNewsDetails(
            @Query("id") long id
    );

    /* Category API ---------------------------------------------------  */
    @Headers({CACHE, AGENT})
    @GET("services/listCategory")
    Call<CallbackCategory> getListCategory();


    /* Product API ---------------------------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("services/listNewestProduct")
    Call<CallbackProduct> getNewestProduct();

    @Headers({CACHE, AGENT})
    @GET("services/listProductNew")
    Call<CallbackProductNew> getProductNew();

    @Headers({CACHE, AGENT})
    @GET("services/listProduct")
    Call<CallbackProduct> getListProduct(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query,
            @Query("category_id") long category_id
    );

    @Headers({CACHE, AGENT})
    @GET("services/getProductDetails")
    Call<CallbackProductDetails> getProductDetails(
            @Query("id") long id
    );


    @Headers({CACHE, AGENT ,SECURITY})
    @GET("services/deleteOneProduct")
    Call<CallbackStatus> deleteOneProduct(
            @Query("id") long id

    );

    @Headers({CACHE, AGENT})
    @POST("services/insertOneProduct")
    Call<CallbackProduct> insertProduct(
            @Body Product product
    );

    @Headers({CACHE, AGENT})
    @GET("services/updateOneProduct")
    Call<CallbackProduct> updateProduct(
            @Query("id") long id,
            @Query("name") String name,
            @Query("price") double price,
            @Query("stock") long stock,
            @Query("description") String description

            );



    /* Checkout API ---------------------------------------------------- */
    @Headers({CACHE, AGENT, SECURITY})
    @POST("services/submitProductOrder")
    Call<CallbackOrder> submitProductOrder(
            @Body Checkout checkout
    );

     /* Login API ---------------------------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("services/loginMerchant")
    Call<CallbackUser> getUser(
            @Query("username") String username,
            @Query("password") String password

            );

    @Headers({CACHE, AGENT})
    @GET("services/submitMerchant")
    Call<CallbackUser> submitUser(
            @Query("username") String username,
            @Query("phone") String phone,
            @Query("email") String email,
            @Query("password") String password

    );
    @Headers({CACHE, AGENT})
    @GET("services/resetMerchant")
    Call<CallbackUser> resetUser(
            @Query("email") String email

    );

    @Headers({CACHE, AGENT})
    @GET("services/updateUser")
    Call<CallbackUser> updateUser(
            @Query("id") String id,
            @Query("name") String name,
            @Query("phone") String phone,
            @Query("email") String email,
            @Query("password") String password

    );

    @Headers({CACHE, AGENT})
    @GET("services/updateMerchant")
    Call<CallbackUser> updateMerchant(
            @Query("id") String id,
            @Query("open") int open,
            @Query("email_tsel") String email_tsel,
            @Query("nik_tsel") String nik_tsel,
            @Query("location") String location,
            @Query("telegram") String telegram

    );

    //chat
    @Headers({CACHE, AGENT})
    @GET("services/getChat")
    Call<CallbackChatList> getChat(
            @Query("page") int page,
            @Query("count") int count,
            @Query("userid") String userid

    );
    @Headers({CACHE, AGENT})
    @GET("services/getAllChatUser")
    Call<CallbackChatDetails> getAllChatUser(
            @Query("page") int page,
            @Query("count") int count,
            @Query("userid") String userid,
            @Query("from") String from
    );

    @Headers({CACHE, AGENT})
    @GET("services/submitChat")
    Call<CallbackChatSubmit> submitChat(
            @Query("from") String from,
            @Query("to") String to,
            @Query("msg") String msg

    );

    @Headers({CACHE, AGENT})
    @GET("services/listProductOrder")
    Call<CallbackProductOrder> getListProductOrder(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query,
            @Query("user_id") String user_id,
            @Query("is_admin") int is_admin,
            @Query("is_merchant") int is_merchant,
            @Query("status") String status
    );



    @Headers({CACHE, AGENT})
    @GET("services/updateProductOrder")
    Call<CallbackStatus> updateProductOrder(
            @Query("id") int id,
            @Query("status") String status


            );

    //////////////////////

    @Headers({CACHE, AGENT})
    @GET("services/listProductSeller")
    Call<CallbackProduct> getListProductSeller(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query,
            @Query("merchant_id") long merchant_id
    );

    //@POST("uploads.php")

    //@Headers({CACHE, AGENT})
    @Multipart
    @POST("services/uploadImage")
    Call<CallbackStatus> upload(
            @Part MultipartBody.Part image,
            @Part("file") RequestBody name
    );


}
