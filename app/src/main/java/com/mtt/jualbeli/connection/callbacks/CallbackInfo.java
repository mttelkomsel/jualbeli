package com.mtt.jualbeli.connection.callbacks;

import com.mtt.jualbeli.model.Info;

import java.io.Serializable;

public class CallbackInfo implements Serializable {
    public String status = "";
    public Info info = null;
}
