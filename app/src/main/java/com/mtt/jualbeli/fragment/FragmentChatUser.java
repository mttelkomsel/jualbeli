package com.mtt.jualbeli.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mtt.jualbeli.ActivityLogin;
import com.mtt.jualbeli.R;
import com.mtt.jualbeli.data.SharedPref;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentChatUser extends Fragment {

    private LinearLayout menuLogout;
    private SharedPref sharedPref;


    public FragmentChatUser() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view=inflater.inflate(R.layout.fragment_chat_user, container, false);
        sharedPref = new SharedPref(getActivity());

       /* menuLogout = (LinearLayout) root_view.findViewById(R.id.menuLogout);
        menuLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPref.clearInfoUser();
                Intent i = new Intent(getActivity(), ActivityLogin.class);
                startActivity(i);
                getActivity().finish();

            }
        });
*/
        return root_view;


    }

}
