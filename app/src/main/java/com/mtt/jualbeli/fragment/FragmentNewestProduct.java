package com.mtt.jualbeli.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.jualbeli.ActivityCategoryDetails;
import com.mtt.jualbeli.ActivityNewestProductDetails;
import com.mtt.jualbeli.ActivityProductDetails;
import com.mtt.jualbeli.ActivityMain;
import com.mtt.jualbeli.R;
import com.mtt.jualbeli.adapter.AdapterProduct;
import com.mtt.jualbeli.adapter.AdapterNewestProduct;
import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackProduct;
import com.mtt.jualbeli.connection.callbacks.CallbackProduct;
import com.mtt.jualbeli.model.Product;
import com.mtt.jualbeli.model.Product;
import com.mtt.jualbeli.utils.AutoFitGridLayoutManager;
import com.mtt.jualbeli.utils.NetworkCheck;
import com.mtt.jualbeli.utils.Tools;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNewestProduct extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private Call<CallbackProduct> callbackCall;
    private AdapterNewestProduct adapter;
    private TextView tvViewAll;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_newest_product, null);
        initComponent();
        requestListProduct();

        return root_view;
    }

    private void initComponent() {
        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
        tvViewAll = (TextView) root_view.findViewById(R.id.textView1);
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));


        recyclerView.setLayoutManager(
                new GridLayoutManager(recyclerView.getContext(), 2, GridLayoutManager.HORIZONTAL, false));
//        AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(recyclerView.getContext(), 270);
//        recyclerView.setLayoutManager(layoutManager);

//        recyclerView.setLayoutManager(
//                new GridLayoutManager(recyclerView.getContext(), Tools.getGridSpanCount(getActivity()), GridLayoutManager.HORIZONTAL, true));
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), Tools.getGridSpanCount(getActivity())));
        recyclerView.setHasFixedSize(true);

        //set data and list adapter
        adapter = new AdapterNewestProduct(getActivity(), new ArrayList<Product>());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener(new AdapterNewestProduct.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Product obj, int position) {
                ActivityProductDetails.navigate(getActivity(), obj.id, false);
            }
        });

        tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"clicked",Toast.LENGTH_LONG).show();
                Intent i = new Intent(getActivity(), ActivityNewestProductDetails.class);
                getActivity().startActivity(i);
            }
        });

    }


    private void requestListProduct() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getNewestProduct();
        callbackCall.enqueue(new Callback<CallbackProduct>() {
            @Override
            public void onResponse(Call<CallbackProduct> call, Response<CallbackProduct> response) {
                CallbackProduct resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.products);
                    ActivityMain.getInstance().newest_product_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackProduct> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }

}
