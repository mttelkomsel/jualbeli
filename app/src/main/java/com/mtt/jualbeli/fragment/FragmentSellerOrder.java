package com.mtt.jualbeli.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.jualbeli.ActivityMain;
import com.mtt.jualbeli.R;
import com.mtt.jualbeli.adapter.AdapterSellerOrder;
import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackProductOrder;
import com.mtt.jualbeli.connection.callbacks.CallbackStatus;
import com.mtt.jualbeli.data.DatabaseHandler;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.ProductOrder;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.NetworkCheck;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mtt.jualbeli.data.Constant.PRODUCT_PER_REQUEST;

public class FragmentSellerOrder extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private AdapterSellerOrder mAdapter;
    private SwipeRefreshLayout swipe_refresh;
    private Call<CallbackProductOrder> callbackCall = null;
    private Call<CallbackStatus> callbackStatus = null;

    private int post_total = 0;
    private int failed_page = 0;

    private SharedPref sharedPref;

    private String userId;
    private String from;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private DatabaseHandler db;
    private Intent intent;

    private RadioGroup radioStatus;
    private RadioButton radioText;
    private Button btnDisplay;
    private UserProfile userProfile;
    private String status;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_seller, null);
        sharedPref = new SharedPref(getActivity());

        //cek login in mainactivity
        userProfile = sharedPref.getInfoUser();
        initComponent();
        status="(" +
                "'"+getString(R.string.proses_bayar)+"',"+
                "'"+getString(R.string.selesai_bayar)+"',"+
                "'"+getString(R.string.proses_kirim)+"',"+
                "'"+getString(R.string.selesai_kirim)+"',"+
                "'"+getString(R.string.batal)+"'"+
                ")";

        return root_view;
    }

    private void initComponent() {
        swipe_refresh = (SwipeRefreshLayout) root_view.findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //set data and list adapter
        mAdapter = new AdapterSellerOrder(getActivity(), recyclerView, new ArrayList<ProductOrder>(), this);
        recyclerView.setAdapter(mAdapter);

        //intent = new Intent(this, ActivitySplash.class);
        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterSellerOrder.OnItemClickListener() {
            @Override
            public void onItemClick(View v, ProductOrder obj, int position) {
                //obj.read = true;
                //intent = ActivityChatDetails.navigateBase(ActivityChatList.this, Long.parseLong(obj.from_id), false, obj.username);
                //startActivity(intent);
                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.must_login), Toast.LENGTH_LONG).show();

            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterSellerOrder.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (post_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });

        requestAction(1);

    }


    private void displayApiResult(final List<ProductOrder> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) {
            showNoItemView(true);
        }
    }

    private void requestProductOrder(final int page_no) {
        API api = RestAdapter.createAPI();
        callbackCall = api.getListProductOrder(page_no, PRODUCT_PER_REQUEST, null, userProfile.id, userProfile.is_admin, 1, status);
        callbackCall.enqueue(new Callback<CallbackProductOrder>() {
            @Override
            public void onResponse(Call<CallbackProductOrder> call, Response<CallbackProductOrder> response) {
                CallbackProductOrder resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    post_total = resp.count_total;
                    displayApiResult(resp.orders);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<CallbackProductOrder> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
    }


    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "");
        showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestProductOrder(page_no);
            }
        }, 2000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) root_view.findViewById(R.id.lyt_failed);
        ((TextView) root_view.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) root_view.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) root_view.findViewById(R.id.lyt_no_item);
        ((TextView) root_view.findViewById(R.id.no_item_message)).setText(R.string.no_item);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }
    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }


    public void dialogUpdateStatus(final ProductOrder order) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_order_status);
        radioStatus = (RadioGroup) dialog.findViewById(R.id.radioStatus);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        //hardcoded status
        RadioButton proses_bayar=(RadioButton) dialog.findViewById(R.id.proses_bayar);
        RadioButton selesai_bayar=(RadioButton) dialog.findViewById(R.id.selesai_bayar);
        RadioButton proses_kirim=(RadioButton) dialog.findViewById(R.id.proses_kirim);
        RadioButton selesai_kirim=(RadioButton) dialog.findViewById(R.id.selesai_kirim);
        RadioButton batal=(RadioButton) dialog.findViewById(R.id.batal);
        RadioButton selesai=(RadioButton) dialog.findViewById(R.id.selesai);

        RadioButton proses_refund=(RadioButton) dialog.findViewById(R.id.proses_refund);
        RadioButton selesai_refund=(RadioButton) dialog.findViewById(R.id.selesai_refund);

        if(userProfile.is_admin==1){
            proses_bayar.setVisibility(View.VISIBLE);
            selesai_bayar.setVisibility(View.VISIBLE);
            proses_kirim.setVisibility(View.VISIBLE);
            selesai_kirim.setVisibility(View.VISIBLE);
            selesai.setVisibility(View.VISIBLE);
            batal.setVisibility(View.VISIBLE);
            proses_refund.setVisibility(View.VISIBLE);
            selesai_refund.setVisibility(View.VISIBLE);

        }else{
            proses_kirim.setVisibility(View.VISIBLE);
            selesai_kirim.setVisibility(View.VISIBLE);
        }


        ((ImageView) dialog.findViewById(R.id.img_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) dialog.findViewById(R.id.code)).setText(order.code);

        ((Button) dialog.findViewById(R.id.bt_negative)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.bt_positive)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dialog.dismiss();

                // get selected radio button from radioGroup
                int selectedId = radioStatus.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioText = (RadioButton) dialog.findViewById(selectedId);

                requestUpdateProductOrder(order.id, radioText.getText().toString(), dialog);
                dialog.dismiss();

            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    private void requestUpdateProductOrder(final int order_id, final String status, final Dialog dialog) {
        API api = RestAdapter.createAPI();
        callbackStatus = api.updateProductOrder(order_id, status);
        callbackStatus.enqueue(new Callback<CallbackStatus>() {
            @Override
            public void onResponse(Call<CallbackStatus> call, Response<CallbackStatus> response) {
                CallbackStatus resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.success_update), Toast.LENGTH_LONG).show();
                    mAdapter.resetListData();
                    requestAction(1);
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.failed_update), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<CallbackStatus> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(1);
            }

        });
    }

}
