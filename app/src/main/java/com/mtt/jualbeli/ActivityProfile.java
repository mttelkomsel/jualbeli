package com.mtt.jualbeli;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.balysv.materialripple.MaterialRippleLayout;
import com.mtt.jualbeli.adapter.AdapterNotification;
import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackUser;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.DatabaseHandler;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.CallbackDialog;
import com.mtt.jualbeli.utils.DialogUtils;
import com.mtt.jualbeli.utils.NetworkCheck;
import com.mtt.jualbeli.utils.Tools;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mtt.jualbeli.R.id.time_shipping;

public class ActivityProfile extends AppCompatActivity {

    public View parent_view;
    private RecyclerView recyclerView;
    private DatabaseHandler db;
    public AdapterNotification adapter;
    static ActivityProfile activityNotification;
    private SharedPref sharedPref;
    private LinearLayout logout,policy_lyt, policy_user_lyt;
    private TextView username,  tv_status_merchant, tv_save_merchant;
    private EditText name,phone, email, password, repassword;
    private EditText email_tsel, nik_tsel, telegram;
    private ToggleButton open;
    private EditText location;
    private CheckBox policy;
    private TextView order_open,order_close;
    private ImageButton bt_order_open, bt_order_close;
    private TimePickerDialog timePickerDialog;

    //private Spinner location;

    private MaterialRippleLayout lyt_save_user;
    private MaterialRippleLayout lyt_save_merchant;

    private UserProfile userProfile;

    public static ActivityProfile getInstance() {
        return activityNotification;
    }

    // activity transition
    public static void navigate(Activity activity) {
        Intent i = new Intent(activity, ActivityProfile.class);
        activity.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(this);

        userProfile = sharedPref.getInfoUser();

        setContentView(R.layout.activity_profile);
        db = new DatabaseHandler(this);

        initToolbar();
        initComponent();


    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_profile);
        Tools.systemBarLolipop(this);
    }

    private void initComponent() {
        parent_view = findViewById(android.R.id.content);
        username = (TextView)findViewById(R.id.username);
        name = (EditText) findViewById(R.id.name);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        repassword = (EditText)findViewById(R.id.repassword);
        lyt_save_user = (MaterialRippleLayout) findViewById(R.id.lyt_save_user);
        lyt_save_merchant = (MaterialRippleLayout) findViewById(R.id.lyt_save_merchant);
        tv_status_merchant = (TextView) findViewById(R.id.tv_status_merchant);
        tv_save_merchant = (TextView) findViewById(R.id.tv_save_merchant);
        logout = (LinearLayout)findViewById(R.id.logout);
        policy_lyt = (LinearLayout)findViewById(R.id.policy_lyt);
        policy_user_lyt = (LinearLayout)findViewById(R.id.policy_user_lyt);
        policy = (CheckBox) findViewById(R.id.policy);

        //merchant
        nik_tsel = (EditText) findViewById(R.id.nik_tsel);
        email_tsel = (EditText)findViewById(R.id.email_tsel);
        telegram = (EditText)findViewById(R.id.telegram);
        //location = (Spinner) findViewById(R.id.location_tsel);
        location = (EditText) findViewById(R.id.location);
        open = (ToggleButton) findViewById(R.id.open);
        order_open = (TextView) findViewById(R.id.order_open);
        order_close = (TextView) findViewById(R.id.order_close);

        bt_order_open = (ImageButton) findViewById(R.id.bt_order_open);
        bt_order_close = (ImageButton) findViewById(R.id.bt_order_close);



        //user profile
        name.setText(userProfile.name);
        username.setText(userProfile.username);
        phone.setText(userProfile.phone);
        email.setText(userProfile.email);

        //merchant profile
        nik_tsel.setText(userProfile.nik_tsel);
        if(userProfile.email_tsel!=null && !userProfile.email_tsel.equals("")){
            String showEmail=userProfile.email_tsel.substring(0,userProfile.email_tsel.indexOf("@"));
            email_tsel.setText(showEmail);

        }
        telegram.setText(userProfile.telegram);
        location.setText(userProfile.location);
        if(userProfile.open>0) open.setChecked(true);
        else open.setChecked(false);


        if(userProfile.is_merchant<1) {
            tv_status_merchant.setText("Belum Aktif");
            tv_save_merchant.setText("AKTIFKAN");

        }else{
            tv_status_merchant.setText("Sudah Aktif");
            tv_save_merchant.setText("SIMPAN");

        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("LoginActivity", "Sign Up Activity activated.");
                sharedPref.clearInfoUser();
                Intent i = new Intent(ActivityProfile.this, ActivityLogin.class);
                startActivity(i);
                finish();

            }
        });
        lyt_save_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProcessUser();

            }
        });
        lyt_save_merchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProcessMerchant();

            }
        });

        //merchant profile
        policy_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialogTerm(ActivityProfile.this);
                ActivityWeb.navigate(ActivityProfile.this, Constant.WEB_TERMS.concat("?type=merchant"), getString(R.string.pref_title_term));
            }
        });

        //merchant profile
        policy_user_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialogTerm(ActivityProfile.this);
                ActivityWeb.navigate(ActivityProfile.this, Constant.WEB_TERMS, getString(R.string.pref_title_term));
            }
        });

        bt_order_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTimePicker(order_open);
            }
        });

        bt_order_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTimePicker(order_close);
            }
        });


    }

    private void dialogTimePicker(final TextView textView) {
        Calendar cur_calender = Calendar.getInstance();
        int mHour = cur_calender.get(Calendar.HOUR_OF_DAY);
        int mMinute = cur_calender.get(Calendar.MINUTE);

        timePickerDialog = new TimePickerDialog(this, R.style.DatePickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay,
                                  int minute) {

                textView.setText(hourOfDay + ":" + minute);
                timePickerDialog.dismiss();
            }
        }, mHour, mMinute, false);
        timePickerDialog.setCancelable(true);
        //timePickerDialog.getTimePicker().setMinDate(System.currentTimeMillis() - 1000);
        timePickerDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.menu_activity_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void dialogTerm(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.pref_title_term));
        builder.setMessage(activity.getString(R.string.content_term));
        builder.setPositiveButton(R.string.OK, null);
        builder.show();
    }

    private void updateProcessUser() {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            String _name = name.getText().toString();
            String _email = email.getText().toString();
            String _phone = phone.getText().toString();
            String _password = password.getText().toString();
            String _repassword = repassword.getText().toString();
            Log.d("PASSWORD",""+_password.isEmpty());
            if (!_name.isEmpty() && !_email.isEmpty() && !_phone.isEmpty() && (!_password.isEmpty() || !_repassword.isEmpty())) {

                if (!_password.equals(_repassword)) {
                    Toast.makeText(getApplicationContext(), getString(R.string.password_not_match), Toast.LENGTH_LONG).show();
                    return;
                }
                if (_password.length() < 5) {
                    Toast.makeText(getApplicationContext(), getString(R.string.invalid_password), Toast.LENGTH_LONG).show();
                    return;
                }

                updateUser(_name, _email, _phone, _password);

            } else if (!_name.isEmpty() && !_email.isEmpty() && !_phone.isEmpty() ) {

                updateUser(_name, _email, _phone);
            } else {

                Toast.makeText(getApplicationContext(), getString(R.string.failed_update), Toast.LENGTH_LONG).show();

            }
        }

    }

    private void updateUser(final String name, final String email, final String phone) {
         updateUser(name, email, phone , null );
    }
    private void updateUser(final String name, final String email, final String phone, final String password) {
        API api = RestAdapter.createAPI();
        Call<CallbackUser> callbackCall = api.updateUser(userProfile.id, name, phone, email, password);
        callbackCall.enqueue(new Callback<CallbackUser>() {
            @Override
            public void onResponse(Call<CallbackUser> call, Response<CallbackUser> response) {
                CallbackUser resp = response.body();
                if (resp != null && resp.status.equals("success") && resp.user!= null) {
                    userProfile = sharedPref.setInfoUser(resp.user);
                    Toast.makeText(getApplicationContext(), getString(R.string.success_update), Toast.LENGTH_LONG).show();
                }else if (resp != null && resp.status.equals("failed") && resp.user!= null) {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_update), Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_update), Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<CallbackUser> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                dialogServerNotConnect();
                //showProgress(false);

            }
        });
    }

    ///////////////////MERCHANT
    private void updateProcessMerchant() {
        if (!NetworkCheck.isConnect(this)) {
            dialogNoInternet();
        } else {
            String _open = open.getTextOn().toString();
            String _email_tsel = email_tsel.getText().toString();

            String _nik_tsel = nik_tsel.getText().toString();
            String _telegram = telegram.getText().toString();
            String _location = location.getText().toString();
            //String _policy = policy.
            boolean _policy = policy.isChecked();

            if (!_email_tsel.isEmpty() && !_nik_tsel.isEmpty() && !_location.isEmpty() && _policy){
                int _status=1;
                if(!_open.equals("BUKA"))_status=0;

                //append email
                if(_email_tsel.indexOf("@telkomsel")!=-1){
                    _email_tsel+="@telkomsel.co.id";
                }

                updateMerchant(_status, _email_tsel, _nik_tsel, _telegram, _location);
             } else {
                Toast.makeText(getApplicationContext(), getString(R.string.failed_update), Toast.LENGTH_LONG).show();

            }
        }

    }

    private void updateMerchant(final int open, final String email_tsel, final String nik_tsel, final String telegram, final String location) {
        API api = RestAdapter.createAPI();
        Call<CallbackUser> callbackCall = api.updateMerchant(userProfile.id, open, email_tsel, nik_tsel, location, telegram);
        callbackCall.enqueue(new Callback<CallbackUser>() {
            @Override
            public void onResponse(Call<CallbackUser> call, Response<CallbackUser> response) {
                CallbackUser resp = response.body();
                if (resp != null && resp.status.equals("success") && resp.user!= null) {
                    userProfile = sharedPref.setInfoUser(resp.user);
                    if(userProfile.is_merchant>0) {
                        Toast.makeText(getApplicationContext(), getString(R.string.success_update), Toast.LENGTH_LONG).show();
                    }else{
                        //Toast.makeText(getApplicationContext(), getString(R.string.success_update), Toast.LENGTH_LONG).show();
                        dialogConfirm();
                    }
                }else if (resp != null && resp.status.equals("failed") && resp.user!= null) {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_update), Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_update), Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<CallbackUser> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                dialogServerNotConnect();
                //showProgress(false);

            }
        });
    }


    public void dialogNoInternet() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_no_internet, R.string.msg_no_internet, R.string.DISMISS, R.string.CLOSE, R.drawable.img_no_internet, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                //retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }
    public void dialogConfirm() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.dialog_title_confirm, R.string.dialog_text_confirm, R.string.OK, R.string.CLOSE, R.drawable.img_checkout_success, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                //retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    public void dialogServerNotConnect() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_unable_connect, R.string.msg_unable_connect, R.string.DISMISS, R.string.CLOSE, R.drawable.img_no_connect, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                //retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    // make a delay to start next activity
    /*private void retryOpenApplication() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateUser(email, password);
            }
        }, 2000);
    }*/

}
