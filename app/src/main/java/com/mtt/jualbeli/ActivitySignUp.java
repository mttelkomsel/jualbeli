package com.mtt.jualbeli;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mtt.jualbeli.connection.API;
import com.mtt.jualbeli.connection.RestAdapter;
import com.mtt.jualbeli.connection.callbacks.CallbackUser;
import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.CallbackDialog;
import com.mtt.jualbeli.utils.DialogUtils;
import com.mtt.jualbeli.utils.NetworkCheck;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Android login screen Activity
 * http://javapapers.com/android/beautiful-android-login-screen-design-tutorial/
 */

public class ActivitySignUp extends AppCompatActivity {

    private View loginFormView;
    private View progressView;
    private AutoCompleteTextView emailTextView;
    private EditText passwordTextView;
    private EditText usernameTextView;
    private EditText phoneTextView;

    private TextView signInText;
    private UserProfile userProfile;
    private SharedPref sharedPref;
    private SwipeRefreshLayout swipe_refresh;
    private View parent_view;
    private LinearLayout policy_lyt;
    private CheckBox policy;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref = new SharedPref(this);


        setContentView(R.layout.activity_signup);
        parent_view = findViewById(android.R.id.content);

        usernameTextView = (EditText) findViewById(R.id.username);
        phoneTextView = (EditText) findViewById(R.id.phone);
        emailTextView = (AutoCompleteTextView) findViewById(R.id.email);

        policy_lyt = (LinearLayout)findViewById(R.id.policy_lyt);
        policy = (CheckBox) findViewById(R.id.policy);

        //loadAutoComplete();
        passwordTextView = (EditText) findViewById(R.id.password);
        passwordTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    initLogin();
                    return true;
                }
                return false;
            }
        });

        Button loginButton = (Button) findViewById(R.id.email_sign_in_button);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                initLogin();
            }
        });

        loginFormView = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);

        //adding underline and link to signup textview
        signInText = (TextView) findViewById(R.id.signInText);
        signInText.setPaintFlags(signInText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Linkify.addLinks(signInText, Linkify.ALL);

        signInText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("ActivitySignIn", "Sign In Activity activated.");
                // this is where you should start the signup Activity
                Intent i = new Intent(ActivitySignUp.this, ActivityLogin.class);
                startActivity(i);
                finish();
            }
        });



        policy_lyt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityWeb.navigate(ActivitySignUp.this, Constant.WEB_TERMS, getString(R.string.pref_title_term));

            }
        });


    }



    /**
     * Validate Login form and authenticate.
     */
    public void initLogin() {

        usernameTextView.setError(null);
        phoneTextView.setError(null);
        emailTextView.setError(null);
        passwordTextView.setError(null);

        String username = usernameTextView.getText().toString();
        String phone = phoneTextView.getText().toString();
        String email = emailTextView.getText().toString();
        String password = passwordTextView.getText().toString();

        boolean cancelLogin = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            passwordTextView.setError(getString(R.string.invalid_password));
            focusView = passwordTextView;
            cancelLogin = true;
        }

        if (TextUtils.isEmpty(username)) {
            usernameTextView.setError(getString(R.string.field_required));
            focusView = usernameTextView;
            cancelLogin = true;
        }
        if (TextUtils.isEmpty(phone)) {
            phoneTextView.setError(getString(R.string.field_required));
            focusView = phoneTextView;
            cancelLogin = true;
        }else if (!isPhoneValid(phone)) {
            phoneTextView.setError(getString(R.string.invalid_phone));
            focusView = phoneTextView;
            cancelLogin = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailTextView.setError(getString(R.string.field_required));
            focusView = emailTextView;
            cancelLogin = true;
        } else if (!isEmailValid(email)) {
            emailTextView.setError(getString(R.string.invalid_email));
            focusView = emailTextView;
            cancelLogin = true;
        }

        boolean _policy = policy.isChecked();
        if(!_policy){
            policy.setError(getString(R.string.field_required));
            focusView = policy;
            cancelLogin = true;
        }

        if (cancelLogin) {
            // error in login
            focusView.requestFocus();
        } else {
            showProgress(true);
            startProcess(username, phone, email, password);

        }
    }

    private boolean isEmailValid(String email) {
        //add your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //add your own logic
        return password.length() > 4;
    }
    private boolean isPhoneValid(String password) {
        //add your own logic
        return password.length() > 8;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }



    private void startProcess(String username, String phone, String email, String password) {
        if (!NetworkCheck.isConnect(this)) {
            //dialogNoInternet();
            dialogServerNotConnect();
        } else {
            requestUser(username, phone, email, password);
        }
    }

    private void requestUser(final String username, final String phone,  final String email, final String password ) {
        API api = RestAdapter.createAPI();
        Call<CallbackUser> callbackCall = api.submitUser(username, phone, email, password);
        callbackCall.enqueue(new Callback<CallbackUser>() {
            @Override
            public void onResponse(Call<CallbackUser> call, Response<CallbackUser> response) {
                CallbackUser resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    /*UserProfile userProfile = sharedPref.setInfoUser(resp.user);
                    showProgress(false);
                    Intent i = new Intent(ActivitySignUp.this, ActivityChat.class);
                    startActivity(i);
                    finish();
                    */
                    showProgress(false);
                    usernameTextView.setText("");
                    emailTextView.setText("");
                    passwordTextView.setText("");
                    dialogSuccess();
                    //Snackbar.make(parent_view, R.string.success_register, Snackbar.LENGTH_SHORT).show();
                   /* Intent i = new Intent(ActivitySignUp.this, ActivityLogin.class);
                    startActivity(i);
                    finish();*/

                    return;

                }else if (resp != null && resp.status.equals("failed")) {
                    showProgress(false);
                    Snackbar.make(parent_view, R.string.failed_register, Snackbar.LENGTH_SHORT).show();
                    return;
                }else {
                    showProgress(false);
                    Snackbar.make(parent_view, R.string.invalid_login , Snackbar.LENGTH_SHORT).show();
                    return;
                }

            }

            @Override
            public void onFailure(Call<CallbackUser> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                dialogServerNotConnect();
                showProgress(false);

            }
        });
    }


    public void dialogSuccess() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_success_register, R.string.success_register, R.string.dialog_login, R.string.CLOSE, R.drawable.img_about, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                //retryOpenApplication();
                Intent i = new Intent(ActivitySignUp.this, ActivityLogin.class);
                startActivity(i);
                finish();

            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }

    public void dialogServerNotConnect() {
        Dialog dialog = new DialogUtils(this).buildDialogWarning(R.string.title_unable_connect, R.string.msg_unable_connect, R.string.TRY_AGAIN, R.string.CLOSE, R.drawable.img_no_connect, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
                //retryOpenApplication();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
                finish();
            }
        });
        dialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
