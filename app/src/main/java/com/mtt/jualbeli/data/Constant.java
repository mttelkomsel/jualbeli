package com.mtt.jualbeli.data;

public class Constant {

    /**
     * -------------------- EDIT THIS WITH YOURS -------------------------------------------------
     */

    // Edit WEB_URL with your url. Make sure you have backslash('/') in the end url
    //public static String WEB_URL = "http://demo.dream-space.web.id/markeet_panel/";

    //public static String WEB_URL = "http://demo.dream-space.web.id/markeet_panel/";
    //public static String WEB_URL = "http://market.mtt.or.id/panel/"; //production

    /*public static String WEB_URL = "http://jualbelimtt.com/admin/"; //production
    public static String WEB_MERCHANT_URL = "http://jualbelimtt.com/merchant/"; //production
    public static String WEB_MERCHANT_FORUM = "https://t.me/joinchat/AAAAAA2KfgjJmFF6zy8LSA"; //production
    public static String WEB_TERMS = "https://jualbelimtt.com/pages/terms.php"; //production
    public static String WEB_PAYMENT = "https://jualbelimtt.com/pages/payment.php"; //production
    */
    /*public static String WEB_URL = "http://192.168.1.13/jualbeli-webadmin/admin/"; //production
    public static String WEB_MERCHANT_URL = "http://192.168.1.13/jualbeli-webadmin/merchant/"; //production
    public static String WEB_MERCHANT_FORUM = "https://t.me/joinchat/AAAAAA2KfgjJmFF6zy8LSA"; //production
    public static String WEB_TERMS = "http://192.168.1.13/jualbeli-webadmin/pages/terms.php"; //production
    public static String WEB_PAYMENT = "http://192.168.1.13/jualbeli-webadmin/pages/payment.php"; //production
      */
    public static String WEB_URL = "http://128.199.190.210/jualbeli-webadmin/admin/"; //production
    public static String WEB_MERCHANT_URL = "http://128.199.190.210/jualbeli-webadmin/merchant/"; //production
    public static String WEB_MERCHANT_FORUM = "https://t.me/joinchat/AAAAAA2KfgjJmFF6zy8LSA"; //production
    public static String WEB_TERMS = "http://128.199.190.210/jualbeli-webadmin/pages/terms.php"; //production
    public static String WEB_PAYMENT = "http://128.199.190.210/jualbeli-webadmin/pages/payment.php"; //production


    public static String TCASH_PHONE = "*637*688*5*";

    /* [ IMPORTANT ] be careful when edit this security code */
    /* This string must be same with security code at Server, if its different android unable to submit order */
    public static final String SECURITY_CODE = "wtWoSaxYJdjHnYBNzPR8tf6AkyvsiaU1cFe1Lsabhc5HErFb4j0UKsU05WsWE9HvtuAS8ejcT62IVl2ILHcY6eeVEqHIYOgug1Ae";

    // Device will re-register the device data to server when open app N-times
    public static int OPEN_COUNTER = 50;


    /**
     * ------------------- DON'T EDIT THIS -------------------------------------------------------
     */

    // this limit value used for give pagination (request and display) to decrease payload
    public static int NEWS_PER_REQUEST = 10;
    public static int PRODUCT_PER_REQUEST = 10;
    public static int NOTIFICATION_PAGE = 20;
    public static int WISHLIST_PAGE = 20;

    // retry load image notification
    public static int LOAD_IMAGE_NOTIF_RETRY = 3;

    // Method get path to image
    public static String getURLimgProduct(String file_name) {
        return WEB_URL + "uploads/product/" + file_name;
    }

    public static String getURLimgNews(String file_name) {
        return WEB_URL + "uploads/news/" + file_name;
    }

    public static String getURLimgCategory(String file_name) {
        return WEB_URL + "uploads/category/" + file_name;
    }


    public static final String BASE_URL = "http://128.199.190.210/gcm_chat/v1";
    public static final String CHAT_THREAD = BASE_URL + "/chat_rooms/_ID_";
    public static final String CHAT_ROOM_MESSAGE = BASE_URL + "/chat_rooms/_ID_/message";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static final String SMS_TCASH_SENDER = "2828";
    public static final String SMS_TCASH_STATUS="TCASH";

}
