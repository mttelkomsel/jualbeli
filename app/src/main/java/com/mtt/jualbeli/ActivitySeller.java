package com.mtt.jualbeli;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.fragment.FragmentCategory;
import com.mtt.jualbeli.fragment.FragmentSellerInvoice;
import com.mtt.jualbeli.fragment.FragmentSellerOrder;
import com.mtt.jualbeli.fragment.FragmentSellerProduct;
import com.mtt.jualbeli.model.Category;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.Tools;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySeller extends AppCompatActivity {

    private static final String EXTRA_CATEGORY_ID = "key.EXTRA_CATEGORY_ID";
    private static final String EXTRA_CATEGORY_NAME = "key.EXTRA_CATEGORY_NAME";

    // activity transition
    public static void navigate(Activity activity, Category category) {
        Intent i = new Intent(activity, ActivitySeller.class);
        i.putExtra(EXTRA_CATEGORY_ID, category.id);
        i.putExtra(EXTRA_CATEGORY_NAME, category.name);
        activity.startActivity(i);
    }

    // activity transition
    public static void navigate(Activity activity) {
        Intent i = new Intent(activity, ActivitySeller.class);
        i.putExtra(EXTRA_CATEGORY_NAME, activity.getString(R.string.ALL));
        activity.startActivity(i);
    }

    private ActionBar actionBar;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private SharedPref sharedPref;
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller);
        //sharedPref = new SharedPref(this);
        //userProfile = sharedPref.getInfoUser();

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_seller);
        Tools.systemBarLolipop(this);
    }


    private void initComponent() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragmentSellerOrder(new FragmentSellerOrder(), "ORDER");
        adapter.addFragmentSellerProduct(new FragmentSellerProduct(), "PRODUCT");
        adapter.addFragmentSellerInvoice(new FragmentSellerInvoice(), "INVOICE");
        //adapter.addFragment(new FragmentSellerOrder(), "Report");
        //adapter.addFragment(new TwoFragment(), "TWO");
        //adapter.addFragment(new ThreeFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragmentSellerOrder(FragmentSellerOrder fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void addFragmentSellerInvoice(FragmentSellerInvoice fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public void addFragmentSellerProduct(FragmentSellerProduct fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }




        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void openMessage(String msg, Long user_id, String username){

            Intent intent = new Intent(ActivitySeller.this, ActivitySplash.class);
            intent = ActivityChatDetails.navigateBase(ActivitySeller.this, user_id, false, username, msg);
            startActivity(intent);
    }

    public void openCall(String phone){

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:0"+phone));
            startActivity(callIntent);

    }

}

