package com.mtt.jualbeli;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.mtt.jualbeli.data.Constant;
import com.mtt.jualbeli.data.SharedPref;
import com.mtt.jualbeli.fragment.FragmentBuyerComplete;
import com.mtt.jualbeli.fragment.FragmentBuyerProgress;
import com.mtt.jualbeli.model.Category;
import com.mtt.jualbeli.model.UserProfile;
import com.mtt.jualbeli.utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class ActivityBuyer extends AppCompatActivity {

    private static final String EXTRA_CATEGORY_ID = "key.EXTRA_CATEGORY_ID";
    private static final String EXTRA_CATEGORY_NAME = "key.EXTRA_CATEGORY_NAME";
    private static final String EXTRA_ORDERID = "key.EXTRA_ORDERID";

    // activity transition
    public static void navigate(Activity activity, Category category) {
        Intent i = new Intent(activity, ActivityBuyer.class);
        i.putExtra(EXTRA_CATEGORY_ID, category.id);
        i.putExtra(EXTRA_CATEGORY_NAME, category.name);
        activity.startActivity(i);
    }

    // activity transition
    public static void navigate(Activity activity) {
        Intent i = new Intent(activity, ActivityBuyer.class);
        i.putExtra(EXTRA_CATEGORY_NAME, activity.getString(R.string.ALL));
        activity.startActivity(i);
    }

    public static void navigate(Activity activity, String orderid) {
        Intent i = new Intent(activity, ActivityBuyer.class);
        i.putExtra(EXTRA_ORDERID, orderid);
        activity.startActivity(i);
    }

    private ActionBar actionBar;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private SharedPref sharedPref;
    private UserProfile userProfile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer);
        //sharedPref = new SharedPref(this);
        //userProfile = sharedPref.getInfoUser();

        //call after submit checkout
        String orderid = getIntent().getStringExtra(EXTRA_ORDERID);
        if(orderid!=null)openPayment(orderid);

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_buyer);
        Tools.systemBarLolipop(this);
    }


    private void initComponent() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragmentBuyerProgress(new FragmentBuyerProgress(), "In Progress");
        adapter.addFragmentBuyerComplete(new FragmentBuyerComplete(), "Completed");
        //adapter.addFragment(new FragmentSellerOrder(), "Produk");
        //adapter.addFragment(new FragmentSellerOrder(), "Tagihan");
        //adapter.addFragment(new FragmentSellerOrder(), "Report");
        //adapter.addFragment(new TwoFragment(), "TWO");
        //adapter.addFragment(new ThreeFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragmentBuyerProgress(FragmentBuyerProgress fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        public void addFragmentBuyerComplete(FragmentBuyerComplete fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void openMessage(String msg, Long user_id, String username){

            Intent intent = new Intent(ActivityBuyer.this, ActivitySplash.class);
            intent = ActivityChatDetails.navigateBase(ActivityBuyer.this, user_id, false, username, msg);
            startActivity(intent);
    }

    public void openCall(String phone){

            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:0"+phone));
            startActivity(callIntent);

    }

   /* public void openCallTcash(String orderid){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+ Constant.TCASH_PHONE+orderid+"%23"));
        startActivity(callIntent);

    }*/
   public void openPayment(String orderid){
       String url=Constant.WEB_PAYMENT;
       ActivityWeb.navigate(ActivityBuyer.this, url +"?orderid="+orderid, "Pembayaran");

   }

}

